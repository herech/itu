<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/presenters/templates/Sign/in.latte

class Templatecda5f23720d53d8f2ac85ccafdc53398 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('94569b80ed', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb344186abb3_content')) { function _lb344186abb3_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>	<div class="row">&nbsp;&nbsp;</div>
	<div class="row">&nbsp;&nbsp;</div>
	<div class="row">
	<div class="small-8 medium-6 large-4 small-centered columns"><h4>Pro vstup do informačního systému veterninární kliniky se prosím přihlašte.</h4></div>
	</div>

	<div class="row">&nbsp;&nbsp;</div>
	<?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["signInForm"], array()) ?>

<?php if ($form->hasErrors()) { ?>	<ul class="errors">
<?php $iterations = 0; foreach ($form->errors as $error) { ?>		<li><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; } ?>
	</ul>
<?php } ?>
	<div class="row">
    <div class="small-8 medium-6 large-4 small-centered columns">
      <label>Login
        <?php echo $_form["login"]->getControl() ?>

      </label>
    </div>
	</div>
  <div class="row">
    <div class="small-8 medium-6 large-4 small-centered columns">
      <label>Heslo
        <?php echo $_form["password"]->getControl() ?>

      </label>
    </div>
  </div>
  <div class="row">
	<div class="small-8 medium-6 large-4 small-centered columns">
	<div class="row">
    <div class="small-4 medium-2 large-1 columns">
		<?php echo $_form["send"]->getControl() ?>

    </div>
	</div>
	</div>
  </div>
<?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>

<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}