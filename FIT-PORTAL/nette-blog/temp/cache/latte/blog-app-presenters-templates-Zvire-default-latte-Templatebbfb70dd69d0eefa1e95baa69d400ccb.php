<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Zvire/default.latte

class Templatebbfb70dd69d0eefa1e95baa69d400ccb extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('28cd4dafb3', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbdc70d4e706_content')) { function _lbdc70d4e706_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5><?php if ($zobraz_souvisejici_zvirata_podle_id_druhu) { ?>

                            Výpis zvířat druhu: <strong><?php echo Latte\Runtime\Filters::escapeHtml($druh->nazev, ENT_NOQUOTES) ?></strong>
<?php } elseif ($zobraz_souvisejici_zvirata_podle_id_majitele) { ?>
                            Výpis zvířat patříci majiteli: <strong><?php echo Latte\Runtime\Filters::escapeHtml($majitel->jmeno, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($majitel->prijmeni, ENT_NOQUOTES) ?></strong>
<?php } elseif ($vyhledej_zvirata) { ?>
                            Vyhledání zvířat podle parametrů: <strong><?php echo Latte\Runtime\Filters::escapeHtml($searchParams, ENT_NOQUOTES) ?></strong>
<?php } else { ?>
                            Výpis zvířat
                        <?php } ?></h5></caption>
            <tr>
                <th>Jméno</th>
                <th>Majitel</th>
                <th>Druh</th>
                <th>Pohlaví</th>
                <th>Barva</th>
                <th>Váha</th>
                <th>Datum narození</th>
                <th>Datum poslední prohlídky</th>
            </tr>

<?php $iterations = 0; foreach ($zvirata as $zvire) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->jmeno, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:default", array('zobrazSouvisejiciLecbyPodleIdZvirete' => $zvire->ID_zvirete)), ENT_COMPAT) ?>
">Zobrazit léčby zvířete</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:edit", array($zvire->ID_zvirete)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:delete", array($zvire->ID_zvirete)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->majitel->jmeno, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($zvire->majitel->prijmeni, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->druh->nazev, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->pohlavi, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->barva, ENT_NOQUOTES) ?></td>
<?php if (isset($zvire->vaha)) { ?>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($zvire->vaha, ENT_NOQUOTES) ?> Kg</td>
<?php } else { ?>
                    <td></td>
<?php } ?>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($template->date($zvire->datum_narozeni, '%d.%m.%Y'), ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($template->date($zvire->dat_posl_prohl, '%d.%m.%Y'), ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}