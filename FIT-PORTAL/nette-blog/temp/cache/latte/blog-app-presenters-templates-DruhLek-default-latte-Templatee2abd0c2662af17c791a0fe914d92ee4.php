<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/DruhLek/default.latte

class Templatee2abd0c2662af17c791a0fe914d92ee4 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('c9dd933f66', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb5542f3c0f8_content')) { function _lb5542f3c0f8_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5><?php if ($zobrazSouvisejiciLekyPodleIdDruhu) { ?>Výpis léků pro druh zvířete: <strong><?php echo Latte\Runtime\Filters::escapeHtml($druh->nazev, ENT_NOQUOTES) ?>
</strong><?php } elseif ($zobrazSouvisejiciDruhyPodleIdLeku) { ?>Výpis druhů zvířat pro lék s názvem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lek->nazev, ENT_NOQUOTES) ?>
</strong> a typem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lek->typ, ENT_NOQUOTES) ?>
</strong><?php } ?></h5></caption>
<?php if ($zobrazSouvisejiciLekyPodleIdDruhu) { ?>
            <tr>
                <th>Název léku</th>
                <th>Typ léku</th>
                <th>Účinná látka</th>
                <th>Kontraindiakce</th>
                <th>Doporučené dávkování</th>
                <th>Vedlejší účinky</th>
            </tr>

<?php $iterations = 0; foreach ($souvisejiciLekyPodleDruhu as $souvisejiciLekPodleDruhu) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->lek->nazev, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:edit", array('upravLekyUDruhu' => 'true', 'ID_leku' => $souvisejiciLekPodleDruhu->ID_leku, 'ID_druhu' => $souvisejiciLekPodleDruhu->ID_druhu)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:deleteLekUDruhu", array('ID_leku' => $souvisejiciLekPodleDruhu->ID_leku, 'ID_druhu' => $souvisejiciLekPodleDruhu->ID_druhu)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </ul>
                   </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->lek->typ, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->lek->ucinna_latka, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->lek->kontraindikace, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->doporucene_davkovani, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleDruhu->vedlejsi_ucinky, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } } elseif ($zobrazSouvisejiciDruhyPodleIdLeku) { ?>
               <tr>
                <th>Název druhu zvířete</th>
                <th>Doporučené dávkování</th>
                <th>Vedlejší účinky</th>
            </tr>

<?php $iterations = 0; foreach ($souvisejiciDruhyPodleIdLeku as $souvisejiciDruhPodleIdLeku) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciDruhPodleIdLeku->druh->nazev, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:edit", array('upravLekyUDruhu' => 'false', 'ID_leku' => $souvisejiciDruhPodleIdLeku->ID_leku, 'ID_druhu' => $souvisejiciDruhPodleIdLeku->ID_druhu)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:deleteDruhULeku", array('ID_leku' => $souvisejiciDruhPodleIdLeku->ID_leku, 'ID_druhu' => $souvisejiciDruhPodleIdLeku->ID_druhu)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciDruhPodleIdLeku->doporucene_davkovani, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciDruhPodleIdLeku->vedlejsi_ucinky, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}