<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Majitel/default.latte

class Template61e58b4ba6c324c55e18a4c646ddda9c extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('85dfc6bde4', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb649b2c3ad2_content')) { function _lb649b2c3ad2_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><div class="row">
<div class="small-12 medium-12 large-12 small-centered columns">
	<div class="row">&nbsp;&nbsp;</div>
	<div class="row">&nbsp;&nbsp;</div>
    <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
    <caption><h5><?php if ($vyhledej_majitele) { ?>

                    Vyhledání majitele podle parametrů: <strong><?php echo Latte\Runtime\Filters::escapeHtml($searchParams, ENT_NOQUOTES) ?></strong>
<?php } else { ?>
                    Výpis majitelů zvířat
                <?php } ?></h5></caption>
    <tr>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>Ulice</th>
        <th>Město</th>
        <th>PSČ</th>
        <th>Číslo účtu</th>
        <th>Rodné číslo</th>
        <th>Číslo OP</th>
        <th>IČO</th>
        <th>DIČ</th>
    </tr>
<?php $iterations = 0; foreach ($majitele as $majitel) { ?>        <tr>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->jmeno, ENT_NOQUOTES) ?>

                <div style="visibility:hidden;" class="div-show-addition-info">
                    <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                    <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                        <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:default", array('zobrazSouvisejiciZvirataPodleIdMajitele' => $majitel->ID_majitele)), ENT_COMPAT) ?>
">Zobrazit zvířata patříci majiteli</a></li>
                        <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:edit", array($majitel->ID_majitele)), ENT_COMPAT) ?>
">Upravit</a></li>
                        <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:delete", array($majitel->ID_majitele)), ENT_COMPAT) ?>
">Smazat</a></li>
                    </ul>
                </div>
            </td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->prijmeni, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->ulice, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->mesto, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->psc, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->cislo_uctu, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->rodne_cislo, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->cislo_OP, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->ICO, ENT_NOQUOTES) ?></td>
            <td><?php echo Latte\Runtime\Filters::escapeHtml($majitel->DIC, ENT_NOQUOTES) ?></td>
        </tr>
<?php $iterations++; } ?>
    </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}