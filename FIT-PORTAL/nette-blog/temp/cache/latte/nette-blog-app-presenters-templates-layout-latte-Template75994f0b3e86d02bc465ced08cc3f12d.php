<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/presenters/templates/@layout.latte

class Template75994f0b3e86d02bc465ced08cc3f12d extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('b3f885931c', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block head
//
if (!function_exists($_b->blocks['head'][] = '_lbcc22b7d6a4_head')) { function _lbcc22b7d6a4_head($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;
}}

//
// block scripts
//
if (!function_exists($_b->blocks['scripts'][] = '_lb8ffeb06f3d_scripts')) { function _lb8ffeb06f3d_scripts($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//nette.github.io/resources/js/netteForms.min.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/main.js"></script>
    
            <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/foundation.min.js"></script>
    <script>
       $(document).foundation();
       $(document).ready(function() {
           
           $(".table-show-additional-info tr").mouseenter(function() {

              $( this ).find( ".div-show-addition-info" ).css("visibility","visible");
           })
          .mouseleave(function() {
              $( this ).find( ".div-show-addition-info" ).css("visibility","hidden");
          });
         
       });

     </script>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">

    <title><?php if (isset($_b->blocks["title"])) { ob_start(); Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'title', $template->getParameters()); echo $template->striptags(ob_get_clean()) ?>
 | <?php } ?>Nette Sandbox</title>

    <link rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/favicon.ico">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/foundation.css">
    <link rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/app.css">
        <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/vendor/modernizr.js"></script>
            <link type="text/css" media="screen" rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/vendor/zurb-responsive-tables/responsive-tables.css">
    <script type="text/javascript" src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/vendor/zurb-responsive-tables/responsive-tables.js"></script>
        

    <?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['head']), $_b, get_defined_vars())  ?>

</head>

<body>
<?php if ($user->isLoggedIn()) { ?>
        <div class="row">
            <div class="small-12 medium-12 large-12 small-centered columns">
                <div class="fixed">
                <nav class="top-bar" data-topbar role="navigation">
                  <ul class="title-area">
                    <li class="name">
                      <h1 style="background:#008CBA"><a style="background:#008CBA" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:default"), ENT_COMPAT) ?>
">&nbsp;&nbsp;&nbsp;Veterinární klinika - Informační systém&nbsp;&nbsp;&nbsp;</a></h1>
                    </li>
                     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
                  </ul>

                  <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                                           
                                           
                        <li><a href="#">Uživatel: <?php echo Latte\Runtime\Filters::escapeHtml($user->getIdentity()->username, ENT_NOQUOTES) ?>
 | Role: <?php echo Latte\Runtime\Filters::escapeHtml($user->getRoles()[0], ENT_NOQUOTES) ?></a></li>                  
                      
                        <li class="active"><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Sign:out"), ENT_COMPAT) ?>">Odhlásit se</a></li>
                    </ul>

                    <!-- Left Nav Section -->
                    <ul class="left">

                      <li class="has-dropdown">
                        <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:default"), ENT_COMPAT) ?>
">Majitelé</a>
                        <ul class="dropdown">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:default"), ENT_COMPAT) ?>
">Výpis majitelů</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:find"), ENT_COMPAT) ?>
">Vyhledat majitele</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Majitel:create"), ENT_COMPAT) ?>
">Přidat majitele</a></li>
                        </ul>
                      </li>

                      <li class="divider"></li>

                      <li class="has-dropdown">
                        <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:default"), ENT_COMPAT) ?>
">Zvířata</a>
                        <ul class="dropdown">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:default"), ENT_COMPAT) ?>
">Výpis zvířat</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:find"), ENT_COMPAT) ?>
">Vyhledat zvíře</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:create"), ENT_COMPAT) ?>
">Přidat zvíře</a></li>
                        </ul>
                      </li>

                      <li class="divider"></li>

                      <li class="has-dropdown">
                        <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:default"), ENT_COMPAT) ?>
">Léčby</a>
                        <ul class="dropdown">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:default"), ENT_COMPAT) ?>
">Výpis léčeb</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:find"), ENT_COMPAT) ?>
">Vyhledat léčbu</a></li>
<?php if ($user->isInRole('Doktor')) { ?>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:create"), ENT_COMPAT) ?>
">Přidat léčbu</a></li>
<?php } ?>
                        </ul>
                      </li>
                      
                      <li class="divider"></li>
                    
                      <li class="has-dropdown">
                        <a href="#">Administrace</a>
                        <ul class="dropdown">
                            <li class="has-dropdown">
                                <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:default"), ENT_COMPAT) ?>
">Spravovat léky</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:default"), ENT_COMPAT) ?>
">Výpis léků</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:find"), ENT_COMPAT) ?>
">Vyhledat lék</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:create"), ENT_COMPAT) ?>
">Přidat lék</a></li>
                                </ul>
                            </li>
                            
                            <li class="has-dropdown">
                                <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Nemoc:default"), ENT_COMPAT) ?>
">Spravovat nemoci</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Nemoc:default"), ENT_COMPAT) ?>
">Výpis nemocí</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Nemoc:find"), ENT_COMPAT) ?>
">Vyhledat nemoc</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Nemoc:create"), ENT_COMPAT) ?>
">Přidat nemoc</a></li>
                                </ul>
                            </li>
                            
                            <li class="has-dropdown">
                                <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Druh:default"), ENT_COMPAT) ?>
">Spravovat druhy</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Druh:default"), ENT_COMPAT) ?>
">Výpis druhů</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Druh:create"), ENT_COMPAT) ?>
">Přidat druh</a></li>
                                </ul>
                            </li>
                            
<?php if ($user->isInRole('Doktor')) { ?>
                            <li class="has-dropdown">
                                <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zamestnanec:default"), ENT_COMPAT) ?>
">Spravovat zaměstnance</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zamestnanec:default"), ENT_COMPAT) ?>
">Výpis zaměstnanců</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zamestnanec:find"), ENT_COMPAT) ?>
">Vyhledat zaměstnance</a></li>
<?php if ($user->isInRole('Vedouci')) { ?>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zamestnanec:create"), ENT_COMPAT) ?>
">Přidat zaměstnance</a></li>
<?php } ?>
                                </ul>
                            </li>
<?php } if ($user->isInRole('Vedouci')) { ?>
                            <li class="has-dropdown">
                                <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Pozice:default"), ENT_COMPAT) ?>
">Spravovat pozice</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Pozice:default"), ENT_COMPAT) ?>
">Výpis pozic</a></li>
                                    <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Pozice:create"), ENT_COMPAT) ?>
">Přidat pozici</a></li>
                                </ul>
                            </li>
<?php } ?>
                        </ul>
                      </li>

                    </ul>
                  </section>
                </nav>
                </div>
            </div>
        </div>
<?php } else { ?>
        
        <div class="row">
        <div class="small-12 medium-12 large-12 small-centered columns">
            <div class="fixed">
            <nav class="top-bar" data-topbar role="navigation">
              <ul class="title-area">
                <li class="name">
                  <h1 style="background:#008CBA"><a href="#" style="background:#008CBA">&nbsp;&nbsp;&nbsp;Veterinární klinika - Informační systém&nbsp;&nbsp;&nbsp;</a></h1>
                </li>
                 <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
              </ul>

              <section class="top-bar-section">

                <!-- Left Nav Section -->
                <ul class="left">
                  <li><a href="#">Přihlášení</a></li>
                </ul>
              </section>
            </nav>
            </div>
        </div>
    </div>
    
<?php } ?>
    <?php $iterations = 0; foreach ($flashes as $flash) { ?>    <div data-alert<?php if ($_l->tmp = array_filter(array($flash->type))) echo ' class="', Latte\Runtime\Filters::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT), '"' ?>
><?php echo Latte\Runtime\Filters::escapeHtml($flash->message, ENT_NOQUOTES) ?><a href="#" class="close">&times;</a></div>
<?php $iterations++; } ?>

<?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'content', $template->getParameters()) ?>

<?php call_user_func(reset($_b->blocks['scripts']), $_b, get_defined_vars())  ?>
</body>
</html>
<?php
}}