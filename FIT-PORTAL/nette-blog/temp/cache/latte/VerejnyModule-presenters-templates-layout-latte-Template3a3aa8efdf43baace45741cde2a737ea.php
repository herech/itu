<?php
// source: E:\Dokumenty\Dropbox\ITU ws\root\app\VerejnyModule\presenters/templates/@layout.latte

class Template3a3aa8efdf43baace45741cde2a737ea extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('20d2e1669f', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block head
//
if (!function_exists($_b->blocks['head'][] = '_lb5aa0f95252_head')) { function _lb5aa0f95252_head($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;
}}

//
// block scripts
//
if (!function_exists($_b->blocks['scripts'][] = '_lbdc9fd336d9_scripts')) { function _lbdc9fd336d9_scripts($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//nette.github.io/resources/js/netteForms.min.js"></script>
    <script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/main.js"></script>
    
        <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/vendor/materialize/js/materialize.min.js"></script>
      <script type="text/javascript">
        $(".button-collapse").sideNav();
        
        $(document).ready(function(){
            $('.slider').slider({ full_width: true, Interval: 7000 });
            $('.slider').slider('next');
            $('.slider').slider('prev');
        });
        
        $(document).ready(function(){
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
        });
         
      </script>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">

    <title><?php if (isset($_b->blocks["title"])) { ob_start(); Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'title', $template->getParameters()); echo $template->striptags(ob_get_clean()) ?>
 | <?php } ?>FIT portál</title>

    <link rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/favicon.ico">

    <style>
    @font-face {
      font-family: 'xxx';
      src: url(<?php echo Latte\Runtime\Filters::escapeCss($basePath) ?>/vendor/fonts/a.ttf) format('truetype');
    }
    </style>
    
          <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto">
      <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/vendor/materialize/css/materialize.min.css"  media="screen,projection">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        

    <?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['head']), $_b, get_defined_vars())  ?>

</head>

<body>
        
    <!-- Dropdown Structure -->
    <ul id="dropdown1" class="dropdown-content">
      <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:drobne"), ENT_COMPAT) ?>
">Drobné projekty</a></li>
      <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:stredni"), ENT_COMPAT) ?>
">Střední projekty</a></li>
      <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:vetsi"), ENT_COMPAT) ?>
">Větší projekty</a></li>
      <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:czEu"), ENT_COMPAT) ?>
">CZ a EU projekty</a></li>
      <li class="divider"></li>
      <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:Sponzoring"), ENT_COMPAT) ?>
">Sponzoring akcí</a></li>
    </ul>
    <div class="row">
    <nav class="light-blue accent-3">
                <a href="#!" class="brand-logo hide-on-med-and-down"><div style="background:white;height:4.25rem;" class="col s1"> <img class="responsive-img" style="display:block;margin:0.75rem auto;" src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/images/FIT_zkratka_barevne_RGB_CZ.png"></div></a>
      <div class="nav-wrapper">
      
      <ul id="nav-mobile" class="left hide-on-med-and-down">

        <a class="brand-logo" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Homepage:default"), ENT_COMPAT) ?>
"><strong  style="font-family: 'Poiret One', sans-serif;font-weight:bold;font-size:2rem;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIT Portál </strong><span style="font-size:2.3rem;">|</span><span style="font-family: 'Poiret One', sans-serif;font-weight:bold;font-size:1.8rem;">|<span style="font-size:1.5rem;font-weight:normal;">|</span><span style="font-size:1rem;font-weight:normal;">|</span><span style="font-size:0.5rem;font-weight:normal;">|</span> <span style="font-size:1.5rem;font-weight:normal;">Spolupráce s FIT přináší výsledky</span></span></a>
      </ul>
      
        <ul class="right">
          <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:JsmeFit:default"), ENT_COMPAT) ?>
">Jsme FIT</a></li>
          <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Vysledky:default"), ENT_COMPAT) ?>
">Výsledky</a></li>
          <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:SprateleneFirmy:default"), ENT_COMPAT) ?>
">Spřátelené Firmy</a></li>
          <!-- Dropdown Trigger -->
          <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Spolupráce<i class="material-icons right">arrow_drop_down</i></a></li>
          <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Homepage:default"), ENT_COMPAT) ?>
"><i class="material-icons">home</i></a></li>
          <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Neverejny:Sign:in"), ENT_COMPAT) ?>
"><i class="material-icons">supervisor_account</i></a></li>
        </ul>
      </div>
    </nav>
    </div>

<?php $iterations = 0; foreach ($flashes as $flash) { ?>    <div data-alert<?php if ($_l->tmp = array_filter(array($flash->type))) echo ' class="', Latte\Runtime\Filters::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT), '"' ?>
><?php echo Latte\Runtime\Filters::escapeHtml($flash->message, ENT_NOQUOTES) ?><a href="#" class="close">&times;</a></div>
<?php $iterations++; } ?>

    
<?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'content', $template->getParameters()) ?>

    
          <footer class="page-footer blue-grey">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Chcete se zapojit do spolupráce?</h5>
                <p class="grey-text text-lighten-4"><a href="#modal-contact" class="btn z-depth-0 modal-trigger" ><i class="material-icons left">message</i>Dejte nám vědět</a></p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Odkazy</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Neverejny:Sign:in"), ENT_COMPAT) ?>
">Neveřejná sekce</a></li>
                  <li><a class="grey-text text-lighten-3" href="https://www.fit.vutbr.cz/.cs">Stránky FIT VUT</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2015 FIT VUT
            <a class="grey-text text-lighten-4 right" href="http://www.fit.vutbr.cz/~beranv/.cs">Koordinátor: Víťa Beran</a>
            </div>
          </div>
        </footer>
        
         <div id="modal-contact" style="max-height: 60%;" class="modal bottom-sheet">
            <div class="modal-content">
              <h4>Kontaktní formulář</h5>
                <div class="row"></div>
                <div class="row">
                  <div class="col s12">


                  <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["kontaktForm"], array()) ?>



                    <div class="row">
                      <div class="input-field col s6">
                        <?php echo $_form["firma"]->getControl() ?>

                        <label for="firma_" style="font-size:1rem;">Firma</label>
                      </div>

                      <div class="input-field col s6">
                        <?php echo $_form["email"]->getControl()->addAttributes(array('class'=>"validate")) ?>

                        <label for="email_" data-error="wrong" data-success="right" style="font-size:1rem;">Email</label>
                      </div>
                    </div>
                      

                   <label for="projekty_" style="font-size:1rem;">Jako firma máme zájem o následující typy projektů:</label>
                    <div class="row">
                      <!-- projdeme všechny jeho položky -->
<?php $iterations = 0; foreach ($form['projekty']->items as $key => $tmp) { ?>
                          <!-- každou položku zobrazíme zvlášť -->
                          <div class="input-field col s2">
                              <input type="checkbox"<?php $_input = $_form["projekty"]; echo $_input->getControlPart($key)->addAttributes(array (
  'type' => NULL,
))->attributes() ?>>
                              <label<?php $_input = $_form["projekty"]; echo $_input->getLabelPart($key)->attributes() ?>
><?php echo Latte\Runtime\Filters::escapeHtml($tmp, ENT_NOQUOTES) ?></label>
                          </div>
<?php $iterations++; } ?>
                    </div>


                    <div class="row"></div>
                    <div class="divider"></div>
                    <div class="row"></div>
                    <div class="row"></div>
                    

                    <div class="row">
                      <div class="input-field col s12">
                        <?php echo $_form["komentar"]->getControl()->addAttributes(array('class'=>"materialize-textarea")) ?>

                        <label for="komentar_" style="font-size:1rem;">Komentář</label>
                      </div>
                    </div>
                    
                    
                    <?php echo $_form["send"]->getControl()->addAttributes(array('class'=>"btn")) ?>



                  <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>



                  </div>
                </div>
            </div>
         </div>
    
<?php call_user_func(reset($_b->blocks['scripts']), $_b, get_defined_vars())  ?>
    
    
</body>
</html>
<?php
}}