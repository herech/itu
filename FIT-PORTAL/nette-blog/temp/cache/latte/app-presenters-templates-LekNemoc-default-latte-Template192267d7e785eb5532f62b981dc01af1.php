<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/LekNemoc/default.latte

class Template192267d7e785eb5532f62b981dc01af1 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('1e34edcc9f', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb8b8951199c_content')) { function _lb8b8951199c_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5><?php if ($zobrazSouvisejiciLekyPodleIdNemoci) { ?>

                            Výpis léků pro nemoc s názvem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($nemoc->nazev, ENT_NOQUOTES) ?></strong>
<?php } elseif ($zobrazSouvisejiciNemociPodleIdLeku) { ?>
                            Výpis nemocí pro lék s názvem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lek->nazev, ENT_NOQUOTES) ?>
</strong> a typem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lek->typ, ENT_NOQUOTES) ?></strong>
                        <?php } ?></h5></caption>
<?php if ($zobrazSouvisejiciLekyPodleIdNemoci) { ?>
                <tr>
                    <th>Název léku</th>
                    <th>Typ léku</th>
                    <th>Účinná látka</th>
                    <th>Kontraindiakce</th>
                    <th>Délka léčby</th>
                    <th>Procento úspěšné léčby</th>
                </tr>

<?php $iterations = 0; foreach ($souvisejiciLekyPodleIdNemoci as $souvisejiciLekPodleIdNemoci) { ?>                <tr>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->lek->nazev, ENT_NOQUOTES) ?>

                        <div style="visibility:hidden;" class="div-show-addition-info">
                            <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                            <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:edit", array('upravLekyUNemoci' => 'true', 'ID_leku' => $souvisejiciLekPodleIdNemoci->ID_leku, 'ID_nemoci' => $souvisejiciLekPodleIdNemoci->ID_nemoci)), ENT_COMPAT) ?>
">Upravit</a></li>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:deleteLekUNemoci", array('ID_leku' => $souvisejiciLekPodleIdNemoci->ID_leku, 'ID_nemoci' => $souvisejiciLekPodleIdNemoci->ID_nemoci)), ENT_COMPAT) ?>
">Smazat</a></li>
                            </ul>
                        </div>
                    </td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->lek->typ, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->lek->ucinna_latka, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->lek->kontraindikace, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->delka_lecby, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleIdNemoci->procento_uspesne_lecby != NULL ? $souvisejiciLekPodleIdNemoci->procento_uspesne_lecby.'%' : NULL, ENT_NOQUOTES) ?></td>
                </tr>
<?php $iterations++; } } elseif ($zobrazSouvisejiciNemociPodleIdLeku) { ?>
               <tr>
                    <th>Název nemoci</th>
                    <th>Příznaky</th>
                    <th>Délka léčby</th>
                    <th>Procento úspěšné léčby</th>
                </tr>

<?php $iterations = 0; foreach ($souvisejiciNemociPodleIdLeku as $souvisejiciNemocPodleIdLeku) { ?>                <tr>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciNemocPodleIdLeku->nemoc->nazev, ENT_NOQUOTES) ?>

                        <div style="visibility:hidden;" class="div-show-addition-info">
                            <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                            <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:edit", array('upravLekyUNemoci' => 'false', 'ID_leku' => $souvisejiciNemocPodleIdLeku->ID_leku, 'ID_nemoci' => $souvisejiciNemocPodleIdLeku->ID_nemoci)), ENT_COMPAT) ?>
">Upravit</a></li>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:deleteNemocULeku", array('ID_leku' => $souvisejiciNemocPodleIdLeku->ID_leku, 'ID_nemoci' => $souvisejiciNemocPodleIdLeku->ID_nemoci)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </div>
                    </td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciNemocPodleIdLeku->nemoc->priznaky, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciNemocPodleIdLeku->delka_lecby, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciNemocPodleIdLeku->procento_uspesne_lecby != NULL ? $souvisejiciNemocPodleIdLeku->procento_uspesne_lecby.'%' : NULL, ENT_NOQUOTES) ?></td>
                </tr>
<?php $iterations++; } } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}