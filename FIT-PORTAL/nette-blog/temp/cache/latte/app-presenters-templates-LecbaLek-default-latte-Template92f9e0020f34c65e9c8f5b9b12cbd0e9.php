<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/LecbaLek/default.latte

class Template92f9e0020f34c65e9c8f5b9b12cbd0e9 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('5e7babfcc9', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb18d8535c20_content')) { function _lb18d8535c20_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>
        
        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
<?php if ($vypisLeky) { ?>
           <caption><h5>Výpis léků předepsaných pro léčbu zvířete se jménem: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lecba->zvire->jmeno, ENT_NOQUOTES) ?>
</strong> v rámci léčby č.: <strong><?php echo Latte\Runtime\Filters::escapeHtml($lecba->poradove_cislo_lecby, ENT_NOQUOTES) ?></strong> </h5></caption>
                <tr>
                    <th>Název léku</th>
                    <th>Typ léku</th>
                    <th>Účinná látka</th>
                    <th>Kontraindiakce</th>
                    <th>Datum nasazení</th>
                    <th>Doba podávání</th>
                    <th>Dávkování</th>
                    <th>Lék podal</th>
                </tr>

<?php $iterations = 0; foreach ($souvisejiciLekyPodleLecby as $souvisejiciLekPodleLecby) { ?>                <tr>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->lek->nazev, ENT_NOQUOTES) ?>

                        <div style="visibility:hidden;" class="div-show-addition-info">
                            
                            <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                            <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
<?php if ($user->isInRole('Doktor')) { ?>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LecbaLek:edit", array('ID_leku' => $souvisejiciLekPodleLecby->ID_leku, 'poradove_cislo_lecby' => $souvisejiciLekPodleLecby->poradove_cislo_lecby, 'ID_zvirete' => $souvisejiciLekPodleLecby->ID_zvirete)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LecbaLek:delete", array('ID_leku' => $souvisejiciLekPodleLecby->ID_leku, 'poradove_cislo_lecby' => $souvisejiciLekPodleLecby->poradove_cislo_lecby, 'ID_zvirete' => $souvisejiciLekPodleLecby->ID_zvirete)), ENT_COMPAT) ?>
">Smazat</a></li>
<?php } ?>
                            <?php if (!$souvisejiciLekPodleLecby->ID_zamestnance) { ?>
<li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LecbaLek:podatLek", array('ID_leku' => $souvisejiciLekPodleLecby->ID_leku, 'poradove_cislo_lecby' => $souvisejiciLekPodleLecby->poradove_cislo_lecby, 'ID_zvirete' => $souvisejiciLekPodleLecby->ID_zvirete)), ENT_COMPAT) ?>
">Podat lék</a></li><?php } ?>

                            </ul>
                            
                            <!-- ID_leku => $souvisejiciLekPodleLecby->ID_leku, poradove_cislo_lecby => $souvisejiciLekPodleLecby->poradove_cislo_lecby, ID_zvirete => souvisejiciLekPodleLecby->ID_zvirete-->
                        </div>
                    </td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->lek->typ, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->lek->ucinna_latka, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->lek->kontraindikace, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($template->date($souvisejiciLekPodleLecby->datum_nasazeni, '%d.%m.%Y'), ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->doba_podavani, ENT_NOQUOTES) ?></td>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->davkovani, ENT_NOQUOTES) ?></td>
                    <td><?php if ($souvisejiciLekPodleLecby->ID_zamestnance) { echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->ref('zamestnanec', 'ID_zamestnance')->jmeno, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($souvisejiciLekPodleLecby->ref('zamestnanec', 'ID_zamestnance')->prijmeni, ENT_NOQUOTES) ;} ?></td>
                </tr>
<?php $iterations++; } } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}