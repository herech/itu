<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/NeverejnyModule/presenters/templates/Projekt/default.latte

class Template13f1bc1a3e85fda87d1422fce84bb662 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('bbf6363556', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbd9c8612e0b_content')) { function _lbd9c8612e0b_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-10 medium-8 large-8 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>
    </div>
    </div>


    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5>Výpis projektů</h5></caption>
            <tr>
                <th>Název</th>
                <th>Typ</th>
                <th>Zastřešující akademik</th>
                <th>Zadavatel</th>
                <th>Rozsah</th>
                <th>Stav</th>
                <th>Max. počet řešitelů</th>
            </tr>

<?php $iterations = 0; foreach ($projekty as $projekt) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->nazev, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Projekt:detail", array($projekt->id_projektu)), ENT_COMPAT) ?>
">Zobrazit Detail</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Projekt:edit", array($projekt->id_projektu)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Projekt:zrusit", array($projekt->id_projektu)), ENT_COMPAT) ?>
">Zrušit projekt</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Projekt:dokoncitProjekt", array($projekt->id_projektu)), ENT_COMPAT) ?>
">Nastavit jako dokončený</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->typ, ENT_NOQUOTES) ?></td>
                <td><?php if ($projekt->id_akademika != NULL) { echo Latte\Runtime\Filters::escapeHtml($projekt->ref('akademik','id_akademika')->login, ENT_NOQUOTES) ;} ?></td>
                <td>
<?php if ($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->typ == 'student') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('student', 'id_studenta')->login, ENT_NOQUOTES) ?>

<?php } else { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('firma','id_firmy')->login, ENT_NOQUOTES) ?>

<?php } ?>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('rozsah_projektu','id_rozsahu_projektu')->rozsah, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->stav, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->max_pocet_prihlasenych, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}