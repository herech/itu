<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/VerejnyModule/presenters/templates/Vysledky/default.latte

class Template7d8e6587aed36a3dd6dff4876e732bf9 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('7e9cf58766', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbd5ed586626_content')) { function _lbd5ed586626_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    
    <style>
    .tabs .indicator {
        background-color:white; /* background-color:#383838; */
        height: 6px;
    }
    
    </style>
    
    <div class="row"></div>
    <div class="container">
        <div class="row">

         <div class="col s12 left-align">
                <span class="flow-text">Úspěšně řešené projekty</span>
            </div>
        </div>
        
        <div class="row"></div>
        
        <div class="row">
            <form action="#">
            <div class="col s1 left-align">
                Filtry:
            </div>
            <div class="col s2 left-align">
                Typ projektu:<br>
                <input type="checkbox" id="typ_projektu_filtr_studentsky">
                <label for="typ_projektu_filtr_studentsky">Studentský</label><br>
                <input type="checkbox" id="typ_projektu_filtr_firemni">
                <label for="typ_projektu_filtr_firemni">Firemní</label>
            </div>
            <div class="col s2 left-align">
                Rozsah:<br>
                    <input type="checkbox" id="rozsah_projektu_filtr_drobny">
                    <label for="rozsah_projektu_filtr_drobny">Drobný</label><br>
                    <input type="checkbox" id="rozsah_projektu_filtr_stredni">
                    <label for="rozsah_projektu_filtr_stredni">Střední</label><br>
                    <input type="checkbox" id="rozsah_projektu_filtr_vetsi">
                    <label for="rozsah_projektu_filtr_vetsi">Větší</label><br>
                    <input type="checkbox" id="rozsah_projektu_filtr_eu">
                    <label for="rozsah_projektu_filtr_eu">Tuzemský a EU</label>
            </div>
            <div class="col s4 left-align">
                Štítky:<br>
                <input placeholder="Seznam štítků oddělených znakem |" id="stitky_seznam_filtr" type="text">
                <label for="stitky_seznam_filtr" class="white-text" style="font-size: 1rem;">Štítky</label>
            </div>
            <div class="col s3 left-align ">
                <input type="submit" class="btn  red" onclick="Materialize.toast('Tato funkcionalita není v bezplatné verzi aplikace implementována.', 4000)" value="Aplikovat Fitry">
            </div>
            </form>
        </div>
        
        <div class="row"></div>
        
          <ul class="collapsible popout" data-collapsible="accordion">
               
<?php $iterations = 0; foreach ($iterator = $_l->its[] = new Latte\Runtime\CachingIterator($projekty) as $projekt) { ?>                  <li>
                  <div<?php if ($_l->tmp = array_filter(array($iterator->odd ? 'blue-grey-text' : NULL,'collapsible-header'))) echo ' class="', Latte\Runtime\Filters::escapeHtml(implode(" ", array_unique($_l->tmp)), ENT_COMPAT), '"' ?>
><strong><?php echo Latte\Runtime\Filters::escapeHtml($projekt->nazev, ENT_NOQUOTES) ?></strong></div>
                  <div class="collapsible-body">
                      <div style="text-align: justify;padding: 2rem; margin:0rem;">
                      <strong>Typ:</strong>
                      <?php echo Latte\Runtime\Filters::escapeHtml($projekt->typ, ENT_NOQUOTES) ?>

                      <br>
                      <strong>Zadavatel:</strong>
                      
<?php if ($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->typ == 'student') { ?>
                            <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('student', 'id_studenta')->login, ENT_NOQUOTES) ?>

<?php } else { ?>
                            <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('firma','id_firmy')->login, ENT_NOQUOTES) ?>

<?php } ?>
                      <br> 
                      <strong>Rozsah:</strong>
                      <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('rozsah_projektu','id_rozsahu_projektu')->rozsah, ENT_NOQUOTES) ?>

                      <br>
                      <strong>Zastřešující akademik:</strong>
                      <?php if ($projekt->id_akademika != NULL) { echo Latte\Runtime\Filters::escapeHtml($projekt->ref('akademik','id_akademika')->login, ENT_NOQUOTES) ;} ?>

                      <br>
                      <strong>Počet řešitelů:</strong>
<?php $projekt_pocet_resitelu = $database->table('student_resitel')->where('projekt_id_projektu = ?',$projekt->id_projektu)->count("*") ?>
                        <?php echo Latte\Runtime\Filters::escapeHtml($projekt_pocet_resitelu, ENT_NOQUOTES) ?>


                      <br>
                      <strong>Zadání:</strong>
                      <?php echo $projekt->popis_zadani ?>

                      <br>
                      <strong>Dosažené výsledky:</strong>
                      <?php echo $projekt->popis_dosazenych_vysledku ?>

                      <br>
                      <strong>Štítky:</strong>
<?php $projekt_stitky = $database->table('projekt_stitky')->where('id_projektu = ?',$projekt->id_projektu) ;$iterations = 0; foreach ($projekt_stitky as $stitek) { ?>                        <span class="chip">
                            <?php echo Latte\Runtime\Filters::escapeHtml($stitek->ref('stitky','id_stitku')->nazev, ENT_NOQUOTES) ?>

                        </span>
<?php $iterations++; } ?>
                      </div>
                  </div>
                  </li>
<?php $iterations++; } array_pop($_l->its); $iterator = end($_l->its) ?>
              </ul>

    </div>
  
  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
  

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}