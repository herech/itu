<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Lecba/default.latte

class Templatef7a4029f1be9a6ea395b8acf94861d38 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('1c0eca6d63', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb85500a9e21_content')) { function _lb85500a9e21_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5><?php if ($zobraz_souvisejici_lecby_podle_id_zvirete) { ?>

                            Výpis léčeb zvířete: <strong><?php echo Latte\Runtime\Filters::escapeHtml($zvire->jmeno, ENT_NOQUOTES) ?></strong>
<?php } elseif ($vyhledej_lecby) { ?>
                            Vyhledání léčeb podle parametrů: <strong><?php echo Latte\Runtime\Filters::escapeHtml($searchParams, ENT_NOQUOTES) ?></strong>
<?php } else { ?>
                            Výpis léčeb
                        <?php } ?></h5></caption>
            <tr>
                <th>Pořadové číslo léčby</th>
                <th>Jméno zvířete</th>
                <th>Majitel</th>
                <th>Nemoc</th>
                <th>Upřesnění diagnózy</th>
                <th>Datum zahájení léčby</th>
                <th>Stav</th>
                <th>Cena</th>
                <th>Diagnózu provedl</th>
            </tr>

<?php $iterations = 0; foreach ($lecby as $lecba) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->poradove_cislo_lecby, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
<?php if ($user->isInRole('Doktor')) { ?>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:edit", array($lecba->ID_zvirete, $lecba->poradove_cislo_lecby)), ENT_COMPAT) ?>
">Upravit</a></li>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lecba:delete", array($lecba->ID_zvirete, $lecba->poradove_cislo_lecby)), ENT_COMPAT) ?>
">Smazat</a></li>
<?php } ?>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LecbaLek:default", array('zobrazSouvisejiciLekyPodleIdZvirete' => $lecba->ID_zvirete, 'zobrazSouvisejiciLekyPodleIdLecby' => $lecba->poradove_cislo_lecby)), ENT_COMPAT) ?>
">Zobrazit léky k dané léčbě</a></li>
<?php if ($user->isInRole('Doktor')) { ?>
                                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LecbaLek:create", array('pridejLekKLecbePodleIDLecby' => $lecba->poradove_cislo_lecby, 'pridejLekKLecbePodleIDZvirete' => $lecba->ID_zvirete)), ENT_COMPAT) ?>
">Předepsat lék k dané léčbě</a></li>
<?php } ?>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->zvire->jmeno, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->zvire->majitel->jmeno, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($lecba->zvire->majitel->prijmeni, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->nemoc->nazev, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->upresneni_diagnozy, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($template->date($lecba->datum_zahajeni_lecby, '%d.%m.%Y'), ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->stav, ENT_NOQUOTES) ?></td>
<?php if (isset($lecba->cena)) { ?>
                    <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->cena, ENT_NOQUOTES) ?> Kč</td>
<?php } else { ?>
                    <td></td>
<?php } ?>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lecba->ref('zamestnanec', 'ID_zamestnance')->jmeno, ENT_NOQUOTES) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($lecba->ref('zamestnanec', 'ID_zamestnance')->prijmeni, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}