<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/VerejnyModule/presenters/templates/Spoluprace/drobne.latte

class Template238ac343b919131cce5d19f837ecc591 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('87f9967412', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbf05e5c9366_content')) { function _lbf05e5c9366_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>  <div class="row"></div>

  <div class="container">
      <div class="row">
        <div class="col s12 left-align">
          <span class="flow-text">Drobné projekty:</span>
        </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col s12" style="text-align: justify;">

        <p>
          Projekty vhodné do předmětů s rozsahem 1-3 měsíce, nízká finanční náročnost, vhodné pro drobné testování a menší problémy, IP firmy.
        </p>

        <p>
          Uvítáme podněty od zájemců vně fakulty na zadávání školních projektů. Pokud se pro projekty podaří získat zájem studentů, mohou být výsledky takových projektů využity ve prospěch zadavatele. Možná je i spolupráce na výuce ve formě hostovaných přednášek ve vyučovaných předmětech nebo celých předmětů zaměřených na získávání profesní specializace.
        </p>

        </br></br>
        <p>
          ZDE BUDE DETAILNĚJŠÍ POPIS TOHOTO DRUHU SPOLUPRÁCE A MOŽNOSTI JAK SE K DANÉMU DRUHU SPOLUPRÁCE PŘIPOJIT.
        </p>
      
      </div>
    </div>
  </div>

  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}