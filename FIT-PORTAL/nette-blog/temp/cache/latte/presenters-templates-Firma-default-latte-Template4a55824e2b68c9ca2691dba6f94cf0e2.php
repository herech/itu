<?php
// source: E:\Dokumenty\Dropbox\ITU ws\root\app\NeverejnyModule\presenters/templates/Firma/default.latte

class Template4a55824e2b68c9ca2691dba6f94cf0e2 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('fc39a24791', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbbf43e53aaa_content')) { function _lbbf43e53aaa_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <h4><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Firma:create"), ENT_COMPAT) ?>
">Přidat firmu</a> | <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Firma:register"), ENT_COMPAT) ?>
">Zastřešit projekt</a> | <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Firma:unregister"), ENT_COMPAT) ?>
">Odzastřešit projekt</a></h4>
    </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5>Výpis firem</h5></caption>
            <tr>
                <th>login</th>
                <th>email</th>
                <th>akademik</th>
            </tr>

<?php $iterations = 0; foreach ($firmy as $firma) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($firma->login, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Firma:edit", array($firma->id_firmy)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Firma:delete", array($firma->id_firmy)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($firma->email, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($firma->akademik->login, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}