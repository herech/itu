<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/NeverejnyModule/presenters/templates/Akademik/default.latte

class Template6d9b60a601a84e5e7228e5df6cff4948 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('9fce7e505f', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb0bf786add7_content')) { function _lb0bf786add7_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <h4><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Akademik:garant"), ENT_COMPAT) ?>
">Garantovat projekt</a> | <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Akademik:ungarant"), ENT_COMPAT) ?>
">Odgarantovat projekt</a></h4>
    </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5>Výpis akademiků</h5></caption>
            <tr>
                <th>login</th>
                <th>email</th>
            </tr>

<?php $iterations = 0; foreach ($akademici as $akademik) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($akademik->login, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Akademik:edit", array($akademik->id_akademika)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Akademik:delete", array($akademik->id_akademika)), ENT_COMPAT) ?>
">Smazat</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($akademik->email, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}