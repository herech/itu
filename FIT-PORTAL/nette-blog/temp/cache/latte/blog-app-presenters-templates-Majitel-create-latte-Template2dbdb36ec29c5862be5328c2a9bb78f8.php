<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Majitel/create.latte

class Template2dbdb36ec29c5862be5328c2a9bb78f8 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('46c4a8f06c', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbda15873f78_content')) { function _lbda15873f78_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div class="small-8 medium-6 large-4 small-centered columns">
        <h4>Přidání majitele</h4>
    </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>

    <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["majitelForm"], array()) ?>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
<?php if ($form->hasErrors()) { ?>            <ul class="errors">
<?php $iterations = 0; foreach ($form->errors as $error) { ?>                <li><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; } ?>
            </ul>
<?php } ?>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Jméno <small>vyžadováno</small>
                <?php echo $_form["jmeno"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Příjmení <small>vyžadováno</small>
                <?php echo $_form["prijmeni"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Ulice
                <?php echo $_form["ulice"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Město
                <?php echo $_form["mesto"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Poštovní směrovací číslo
                <?php echo $_form["psc"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Číslo účtu
                <?php echo $_form["cislo_uctu"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Rodné číslo
                <?php echo $_form["rodne_cislo"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Číslo občanského průkazu
                <?php echo $_form["cislo_OP"]->getControl() ?>

            </label>
        </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <label>Typ <small>vyžadováno</small>
            <div class="row">
<?php $iterations = 0; foreach ($form['typ']->items as $key => $label) { ?>
                <div class="small-6 medium-4 large-4 columns end">
                    <label<?php $_input = $_form["typ"]; echo $_input->getLabelPart($key)->attributes() ?>
><input<?php $_input = $_form["typ"]; echo $_input->getControlPart($key)->attributes() ?>
> <?php echo Latte\Runtime\Filters::escapeHtml($label, ENT_NOQUOTES) ?></label>
                </div>
<?php $iterations++; } ?>
            </div>
            </label>
        </div>
        </div>

        <div id="cont_pravnicka_osoba">
            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>IČO
                    <?php echo $_form["ICO"]->getControl() ?>

                </label>
            </div>
            </div>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>DIC
                    <?php echo $_form["DIC"]->getControl() ?>

                </label>
            </div>
            </div>
        </div>

        <div class="row">
        <div class="small-8 medium-6 large-4 small-centered columns">
            <div class="row">
            <div class="small-4 medium-2 large-1 columns">
                <?php echo $_form["send"]->getControl() ?>

            </div>
            </div>
        </div>
        </div>
        
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>
    <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}