<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/NeverejnyModule/presenters/templates/Rozsah/default.latte

class Template1b211c74daa0547b44d9bf5634b1afc9 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('fe3d2ff513', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb08cfb83d90_content')) { function _lb08cfb83d90_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>
        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5>Výpis rozsahů projektů</h5></caption>
            <tr>
                <th>rozsah</th>
                <th>abstrakt</th>
                <th>popis</th>
            </tr>

<?php $iterations = 0; foreach ($rozsahy as $rozsah) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($rozsah->rozsah, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Rozsah:edit", array($rozsah->id_rozsah_projektu)), ENT_COMPAT) ?>
">Upravit</a>
                    </div>
                </td>
                <td><?php echo $rozsah->abstrakt ?></td>
                <td><?php echo $rozsah->popis ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}