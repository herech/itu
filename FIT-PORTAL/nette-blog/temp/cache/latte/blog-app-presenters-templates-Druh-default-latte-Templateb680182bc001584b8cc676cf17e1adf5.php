<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Druh/default.latte

class Templateb680182bc001584b8cc676cf17e1adf5 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('2484f09739', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbc9ab5d85f1_content')) { function _lbc9ab5d85f1_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><div class="row">
<div class="small-12 medium-12 large-12 small-centered columns">
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>

    <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
    <caption><h5>Výpis druhů zvířat</h5></caption>
    <tr>
        <th>Název</th>
    </tr>
<?php $iterations = 0; foreach ($druhy as $druh) { ?>    <tr class="druh">
        <td><?php echo Latte\Runtime\Filters::escapeHtml($druh->nazev, ENT_NOQUOTES) ?>

            <div style="visibility:hidden;" class="div-show-addition-info">
            <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
            <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Druh:edit", array($druh->ID_druhu)), ENT_COMPAT) ?>
">Upravit</a></li>
                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Druh:delete", array($druh->ID_druhu)), ENT_COMPAT) ?>
">Smazat</a></li>
                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Zvire:default", array('zobrazSouvisejiciZvirataPodleIdDruhu' => $druh->ID_druhu)), ENT_COMPAT) ?>
">Zobrazit zvířata tohoto druhu</a></li>
                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:default", array('zobrazSouvisejiciLekyPodleIdDruhu' => $druh->ID_druhu)), ENT_COMPAT) ?>
">Zobrazit léky k danému druhu</a></li>
                <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:create", array('pridejLekKeDruhuPodleID' => $druh->ID_druhu)), ENT_COMPAT) ?>
">Přidat lék k danému druhu</a></li>
            </ul>
            </div>
        </td>
    </tr>
<?php $iterations++; } ?>
    </table>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}