<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/NeverejnyModule/presenters/templates/Homepage/student.latte

class Template11d9b94ab3c995321e04cdc6b038aed7 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('3ee52e6530', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb218c063b34_content')) { function _lb218c063b34_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>	<div class="row">&nbsp;&nbsp;</div>
	<div class="row">&nbsp;&nbsp;</div>
	<div class="row">
	<div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <h4>Profil studenta</br></h4>
    </div>
    </div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Login: <?php echo Latte\Runtime\Filters::escapeHtml($student->login, ENT_NOQUOTES) ?></br>
            email: <?php echo Latte\Runtime\Filters::escapeHtml($student->email, ENT_NOQUOTES) ?>

            </h5>
        </div>
    </div>
    </div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Vytvořené projekty:</br>
            </h5>

            <ul>
<?php $iterations = 0; foreach ($vytvorene_projekty as $v_projekt) { ?>                <li>
                    <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Neverejny:Projekt:detail", array($v_projekt->id_projektu)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($v_projekt->nazev, ENT_NOQUOTES) ?></a>
                </li>
<?php $iterations++; } ?>
            </ul>
        </div>
    </div>
    </div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Zapsané projekty:</br>
            </h5>

            <ul>
<?php $iterations = 0; foreach ($zapsane_projekty as $z_projekt) { ?>                <li>
                    <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Neverejny:Projekt:detail", array($z_projekt->id_projektu)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($z_projekt->nazev, ENT_NOQUOTES) ?></a>
                </li>
<?php $iterations++; } ?>
            </ul>
        </div>
    </div>
    </div>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}