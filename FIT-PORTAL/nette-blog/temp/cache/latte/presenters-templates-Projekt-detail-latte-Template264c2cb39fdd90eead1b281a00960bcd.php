<?php
// source: E:\Dokumenty\Dropbox\ITU ws\root\app\NeverejnyModule\presenters/templates/Projekt/detail.latte

class Template264c2cb39fdd90eead1b281a00960bcd extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('a1a387a408', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbffe6cd08f9_content')) { function _lbffe6cd08f9_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <script type="text/javascript">
    $(document).ready(function() {
        $( "#toggle-rating" ).click(function() {
          $( "#project-rating" ).fadeToggle();
          $( "#projekt-technicka-zprava" ).fadeOut();
        });
         $( "#toggle-technicka-zprava" ).click(function() {
          $( "#project-technicka-zprava" ).fadeToggle();
          $( "#project-rating" ).fadeOut();
        });
    });
    </script>
            <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>
    <div class="row">
    <div class="small-10 medium-8 large-8 small-centered columns">

        <h4><a id="toggle-rating"><?php if ($hodnoceni) { ?>Přidat hodnocení<?php } ?>
</a></h4> <h4><a id="toggle-technicka-zprava"><?php if ($nahrani_zpravy) { ?>| Přidat technickou zprávu<?php } ?></a></h4>
    </div>
    </div>
    <div id="project-rating" style="display: none" class="row">
            <div class="small-8 medium-6 large-4 small-centered columns"><h4>Přidání hodnocení k projektu</h4></div>
        <div class="small-8 medium-6 large-4 small-centered columns">
            <table>
            <tr>
            <td>Ohodnoťe spolupráci počtem hvězdiček:</td> <td><div data-number="5" id="stars-rating"></div></td>
            </tr>
            </table>
            <script type="text/javascript">
           
            $('div#stars-rating').raty({
              click: function(score, evt) {
                $("input[name='znamka']").val(score);
              },
              starOff: <?php echo Latte\Runtime\Filters::escapeJs($basePath) ?> + '/vendor/raty-2.7.0/lib/images/' + 'star-off.png',
              starOn : <?php echo Latte\Runtime\Filters::escapeJs($basePath) ?> + '/vendor/raty-2.7.0/lib/images/' + 'star-on.png' 
            });
         
            </script>
<?php $_l->tmp = $_control->getComponent("pridejHodnoceniForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>
        </div>
    </div>
        <div id="project-technicka-zprava" style="display: none" class="row">
            <div class="small-8 medium-6 large-4 small-centered columns"><h4>Přidání technické zprávy k projektu</h4></div>
        <div class="small-8 medium-6 large-4 small-centered columns">
<?php $_l->tmp = $_control->getComponent("pridejTechnickouZpravuForm"); if ($_l->tmp instanceof Nette\Application\UI\IRenderable) $_l->tmp->redrawControl(NULL, FALSE); $_l->tmp->render() ?>
        </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div class="small-10 medium-8 large-8 small-centered columns">

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5>Detail projektu</h5></caption>

            <tr>
                <th>Název</th> <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->nazev, ENT_NOQUOTES) ?></td>
            </tr>
            <tr>
                <th>Typ</th> <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->typ, ENT_NOQUOTES) ?></td>
            </tr>
            <tr>
                <th>Zastřešující akademik</th> <td><?php if ($projekt->id_akademika != NULL) { echo Latte\Runtime\Filters::escapeHtml($projekt->ref('akademik','id_akademika')->login, ENT_NOQUOTES) ;} ?></td>
            </tr>
            <tr>
                <th>Zadavatel</th> 
                <td>
<?php if ($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->typ == 'student') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('student', 'id_studenta')->login, ENT_NOQUOTES) ?>

<?php } elseif ($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->typ == 'firma') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('zadavatel_hodnotitel','id_zadavatele')->ref('firma','id_firmy')->login, ENT_NOQUOTES) ?>

<?php } ?>
                </td>
            </tr>
            <tr>
                <th>Rozsah</th> <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->ref('rozsah_projektu','id_rozsahu_projektu')->rozsah, ENT_NOQUOTES) ?></td>
            </tr>
            <tr>
                <th>Stav</th> <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->stav, ENT_NOQUOTES) ?></td>
            </tr>
            <tr>
                <th>Max. počet řešitel</th> <td><?php echo Latte\Runtime\Filters::escapeHtml($projekt->max_pocet_prihlasenych, ENT_NOQUOTES) ?></td>
            </tr>
            <tr>
                <th>Popis zadání</th> <td><?php echo $projekt->popis_zadani ?></td>
            </tr>
            <tr>
                <th>Popis požadovaných výsledků</th> <td><?php echo $projekt->popis_pozadovanych_vysledku ?></td>
            </tr>
            <tr>
                <th>Popis dosažených výsledků</th> <td><?php echo $projekt->popis_dosazenych_vysledku ?></td>
            </tr>
        </table>
        
        <div>
        
            <em>Štítky:</em><br ú>
<?php $iterations = 0; foreach ($projekt_stitky as $stitek) { ?>            <span class="round label">
                <?php echo Latte\Runtime\Filters::escapeHtml($stitek->ref('stitky','id_stitku')->nazev, ENT_NOQUOTES) ?>

            </span>
<?php $iterations++; } ?>
        </div>
        <div>
            <br><em>Technické zprávy:</em>
<?php $iterations = 0; foreach ($projekt_resitel as $projekt_resitell) { ?>            <div>
                <?php echo Latte\Runtime\Filters::escapeHtml($projekt_resitell->ref('student','student_id_studenta')->login, ENT_NOQUOTES) ?>
: <a href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>
/images/<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($template->escapeurl($projekt_resitell->cesta_k_technicke_zprave)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($projekt_resitell->cesta_k_technicke_zprave, ENT_NOQUOTES) ?></a>
            </div>
<?php $iterations++; } ?>
        </div>
        <div>
            <br><em>Hodnocení spolupráce:</em> 
<?php $iterations = 0; foreach ($hodnoceni_komentare as $hodnocenii) { ?>            <div style="padding-top: 0.5rem;padding-bottom:0.5rem;border-bottom: 1px dotted gray;">
<?php if ($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->typ == 'student') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->ref('student', 'id_studenta')->login, ENT_NOQUOTES) ?>

<?php } elseif ($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->typ == 'firma') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->ref('firma','id_firmy')->login, ENT_NOQUOTES) ?>

<?php } elseif ($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->typ == 'akademik') { ?>
                    <?php echo Latte\Runtime\Filters::escapeHtml($hodnocenii->ref('zadavatel_hodnotitel', 'id_zadavatele_hodnotitele')->ref('akademik','id_akademika')->login, ENT_NOQUOTES) ?>

                <?php } ?>: 
                <div data-number="5" id="stars-rating<?php echo Latte\Runtime\Filters::escapeHtml($hodnocenii->id_zadavatele_hodnotitele, ENT_COMPAT) ?>"></div>
                <div><?php echo Latte\Runtime\Filters::escapeHtml($hodnocenii->komentar, ENT_NOQUOTES) ?></div>

                <script type="text/javascript">
               
                $('div#stars-rating' + <?php echo Latte\Runtime\Filters::escapeJs($hodnocenii->id_zadavatele_hodnotitele) ?>).raty({
                  readOnly: true,
                  score: <?php echo Latte\Runtime\Filters::escapeJs($hodnocenii->znamka) ?>,
                  starOff: <?php echo Latte\Runtime\Filters::escapeJs($basePath) ?> + '/vendor/raty-2.7.0/lib/images/' + 'star-off.png',
                  starOn : <?php echo Latte\Runtime\Filters::escapeJs($basePath) ?> + '/vendor/raty-2.7.0/lib/images/' + 'star-on.png' 
                });
             
                </script>
            </div>
<?php $iterations++; } ?>
        </div>
    </div>
    </div>
    
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
    
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}