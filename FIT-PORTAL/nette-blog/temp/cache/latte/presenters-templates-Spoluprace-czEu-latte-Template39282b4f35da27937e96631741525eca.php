<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/VerejnyModule/presenters/templates/Spoluprace/czEu.latte

class Template39282b4f35da27937e96631741525eca extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('eb618a9dbc', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbe125e64599_content')) { function _lbe125e64599_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>  <div class="row"></div>

  <div class="container">
      <div class="row">
        <div class="col s12 left-align">
          <span class="flow-text">Tuzemské i Evropské projekty:</span>
        </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col s12" style="text-align: justify;">

        <p>
          Spolupráce na tuzemských i Evropských projektech.
        </p>

        <p>
          Fakulta informačních technologií je velmi úspěšná v získávání a řešení výzkumných projektů národních (MPO, MV, GAČR apod.) i mezinárodních (FP7 EU a FP7-ARTEMIS EU). Pro výzkum hledáme partnery z řad průmyslových podniků, kteří mají zájem o výsledky programu a jsou ochotni se na výzkumu podílet.
        </p>

        <p>
          Vědecko výzkumná, vývojová a publikační činnost je velmi důležitou specifickou součástí činností zajišťovaných FIT. Poskytuje prostor pro tvůrčí základní i aplikovaný výzkum, vývoj prototypů, národní a mezinárodní spolupráci, je nezbytná pro vědecký a odborný růst, je podmínkou habilitací docentů, jmenování profesorů a obhájení disertací studentů doktorského studijního programu. Je rovněž významným ekonomickým zdrojem zajišťujícím rozvoj fakulty.
        </p>

        <p>
          Vědecká a výzkumná činnost je podstatnou součástí doktorského studijního programu a účastní se jí i nadaní studenti magisterského, případně bakalářského studijního programu. Je převážně realizována v rámci Výzkumného záměru fakulty, grantových a výzkumných projektů, habilitačních prací a disertačních prací studentů doktorského studijního programu.
        </p>

        </br></br>
        <p>
          ZDE BUDE DETAILNĚJŠÍ POPIS TOHOTO DRUHU SPOLUPRÁCE A MOŽNOSTI JAK SE K DANÉMU DRUHU SPOLUPRÁCE PŘIPOJIT.
        </p>
      
      </div>
    </div>
  </div>

  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}