<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/LekNemoc/create.latte

class Template55de4b3464f0081915b2f621c683f054 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('1351c9719b', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbbe866b71db_content')) { function _lbbe866b71db_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div class="small-8 medium-6 large-4 small-centered columns">
        <h4><?php if ($pridejLekKNemociPodleID) { ?>Přidání léku k nemoci: <?php echo Latte\Runtime\Filters::escapeHtml($nemoc->nazev, ENT_NOQUOTES) ;} elseif ($pridejNemocKLekuPodleID) { ?>
Přidání nemoci k léku: <?php echo Latte\Runtime\Filters::escapeHtml($lek->nazev, ENT_NOQUOTES) ;} ?></h4>
    </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>
<?php if ($pridejLekKNemociPodleID) { ?>
        <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["pridejLekKNemociForm"], array()) ?>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
<?php if ($form->hasErrors()) { ?>                <ul class="errors">
<?php $iterations = 0; foreach ($form->errors as $error) { ?>                    <li><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; } ?>
                </ul>
<?php } ?>
            </div>
            </div>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Délka léčby
                    <?php echo $_form["delka_lecby"]->getControl() ?>

                </label>
            </div>
            </div>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Procento úspěšné léčby
                    <?php echo $_form["procento_uspesne_lecby"]->getControl() ?>

                </label>
            </div>
            </div>

            <?php echo $_form["ID_nemoci"]->getControl() ?>


            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Léky <small>vyžadováno</small>
                    <?php echo $_form["ID_leku"]->getControl() ?>

                </label>
            </div>
            </div>
            

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <div class="row">
                <div class="small-4 medium-2 large-1 columns">
                    <?php echo $_form["send"]->getControl() ?>

                </div>
                </div>
            </div>
            </div>

            <div class="row">&nbsp;&nbsp;</div>
            <div class="row">&nbsp;&nbsp;</div>
        <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>

<?php } elseif ($pridejNemocKLekuPodleID) { ?>
        <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["pridejNemocKLekuForm"], array()) ?>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
<?php if ($form->hasErrors()) { ?>                <ul class="errors">
<?php $iterations = 0; foreach ($form->errors as $error) { ?>                    <li><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; } ?>
                </ul>
<?php } ?>
            </div>
            </div>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Délka léčby
                    <?php echo $_form["delka_lecby"]->getControl() ?>

                </label>
            </div>
            </div>

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Procento úspěšné léčby
                    <?php echo $_form["procento_uspesne_lecby"]->getControl() ?>

                </label>
            </div>
            </div>

            <?php echo $_form["ID_leku"]->getControl() ?>


            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <label>Nemoci <small>vyžadováno</small>
                    <?php echo $_form["ID_nemoci"]->getControl() ?>

                </label>
            </div>
            </div>
            

            <div class="row">
            <div class="small-8 medium-6 large-4 small-centered columns">
                <div class="row">
                <div class="small-4 medium-2 large-1 columns">
                    <?php echo $_form["send"]->getControl() ?>

                </div>
                </div>
            </div>
            </div>

            <div class="row">&nbsp;&nbsp;</div>
            <div class="row">&nbsp;&nbsp;</div>
        <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>

<?php } 
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}