<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/NeverejnyModule/presenters/templates/Homepage/akademik.latte

class Template37deab36f00d0c57e3bdd92f005c49d7 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('8f9ae0ec86', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb63ef5c1108_content')) { function _lb63ef5c1108_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <h4>Profil akademika</br></h4>
    </div>
    </div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Login: <?php echo Latte\Runtime\Filters::escapeHtml($akademik->login, ENT_NOQUOTES) ?></br>
            email: <?php echo Latte\Runtime\Filters::escapeHtml($akademik->email, ENT_NOQUOTES) ?>

            </h5>
        </div>
    </div>
    </div>
    <div class="row">&nbsp;&nbsp;</div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Garantované projekty:</br>
            </h5>

            <ul>
<?php $iterations = 0; foreach ($garantovane_projekty as $g_projekt) { ?>                <li>
                    <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Neverejny:Projekt:detail", array($g_projekt->id_projektu)), ENT_COMPAT) ?>
"><?php echo Latte\Runtime\Filters::escapeHtml($g_projekt->nazev, ENT_NOQUOTES) ?></a>
                </li>
<?php $iterations++; } ?>
            </ul>
        </div>
    </div>
    </div>

    <div class="row">
    <div style="text-align:center;" class="small-10 medium-8 large-8 small-centered columns">
        <div style="text-align:left;" class="small-12 medium-12 large-12 small-centered columns">
            <h5>
            Odpovídá za firmy:</br>
            </h5>

            <ul>
<?php $iterations = 0; foreach ($odp_firmy as $o_firma) { ?>                <li>
                    <?php echo Latte\Runtime\Filters::escapeHtml($o_firma->login, ENT_NOQUOTES) ?>

                </li>
<?php $iterations++; } ?>
            </ul>
        </div>
    </div>
    </div>

<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}