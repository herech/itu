<?php
// source: /www/sites/4/site22884/public_html/nette-blog/app/presenters/templates/Lek/default.latte

class Template11a4a92d1bad7fa69db5ca72d75cf709 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('a716045044', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb681d9f2e64_content')) { function _lb681d9f2e64_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row">
    <div class="small-12 medium-12 large-12 small-centered columns">
        <div class="row">&nbsp;&nbsp;</div>
        <div class="row">&nbsp;&nbsp;</div>

        <table class="table-show-additional-info" class="responsive" style="margin-left:auto;margin-right:auto;width:100%">
            <caption><h5><?php if ($vyhledej_lek) { ?>

                            Vyhledání léku podle parametrů: <strong><?php echo Latte\Runtime\Filters::escapeHtml($searchParams, ENT_NOQUOTES) ?></strong>
<?php } else { ?>
                            Výpis léků
                        <?php } ?></h5></caption>
            <tr>
                <th>Název</th>
                <th>Typ</th>
                <th>Účinná látka</th>
                <th>Kontraindikace</th>
            </tr>
            
<?php $iterations = 0; foreach ($leky as $lek) { ?>            <tr>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lek->nazev, ENT_NOQUOTES) ?>

                    <div style="visibility:hidden;" class="div-show-addition-info">
                        <a data-dropdown="drop1" aria-controls="drop1" aria-expanded="false">Související akce</a>
                        <ul id="drop1" class="f-dropdown" data-dropdown-content aria-hidden="true" tabindex="-1">
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:edit", array($lek->ID_leku)), ENT_COMPAT) ?>
">Upravit</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Lek:delete", array($lek->ID_leku)), ENT_COMPAT) ?>
">Smazat</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:default", array('zobrazSouvisejiciDruhyPodleIdLeku' => $lek->ID_leku)), ENT_COMPAT) ?>
">Zobrazit druhy zvířat k danému léku</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("DruhLek:create", array('pridejDruhKLekuPodleID' => $lek->ID_leku)), ENT_COMPAT) ?>
">Přidat druh zvířete k danému léku</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:default", array('zobrazSouvisejiciNemociPodleIdLeku' => $lek->ID_leku)), ENT_COMPAT) ?>
">Zobrazit nemoci k danému léku</a></li>
                            <li><a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("LekNemoc:create", array('pridejNemocKLekuPodleID' => $lek->ID_leku)), ENT_COMPAT) ?>
">Přidat nemoc k danému léku</a></li>
                        </ul>
                    </div>
                </td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lek->typ, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lek->ucinna_latka, ENT_NOQUOTES) ?></td>
                <td><?php echo Latte\Runtime\Filters::escapeHtml($lek->kontraindikace, ENT_NOQUOTES) ?></td>
            </tr>
<?php $iterations++; } ?>
        </table>
    </div>
    </div>

    <div class="row">&nbsp;&nbsp;</div>
    <div class="row">&nbsp;&nbsp;</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}