<?php
// source: E:\Dokumenty\Dropbox\ITU ws\root\app\VerejnyModule\presenters/templates/Spoluprace/cZEU.latte

class Templatebd38de8ff6432891089d5b74a99de75b extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('5a09c04233', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb014c23b84c_content')) { function _lb014c23b84c_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <div class="row"></div>

    <div class="container">
        <div class="row">
          <div class="col s12 left-align">
              <span class="flow-text">Spřátelené firmy FIT VUT:</span>
          </div>
      </div>
    </div>

  <div class="container">
    <div class="row">
      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/IBM_logo.svg/2000px-IBM_logo.svg.png">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Společnost IBM je největším poskytovatelem řešení a služeb informačních technologií na světě s dlouhou tradicí inovací<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="http://www.ibm.com/cz-cs/">IBM</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
            <span class="card-title">IBM<i class="material-icons right">close</i></span>
              <p>Společnost IBM je největším poskytovatelem řešení a služeb informačních technologií na světě s dlouhou tradicí inovací. První pobočka IBM v České republice byla založena již v roce 1932 jako první pobočka v regionu střední a východní Evropy a jako šestá v Evropě. Převažující činností IBM v České republice je prodej širokého spektra IT technologií od serverů a systémů pro ukládání dat až po software a IT služby.</p>
            </div>
          </div>

        </div>
      </div>


      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="https://upload.wikimedia.org/wikipedia/commons/9/95/Mozilla_Corporation_logo.svg">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Mozilla Corporation je společnost, která je plně vlastněna Mozilla Foundation a která zajišťuje vývoj, distribuci a propagaci internetových aplikací<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="https://www.mozilla.org/cs/">mozilla</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
              <span class="card-title">Mozilla Corporation<i class="material-icons right">close</i></span>
              <p>Mozilla Corporation je společnost, která je plně vlastněna Mozilla Foundation a která zajišťuje vývoj, distribuci a propagaci internetových aplikací jako webový prohlížeč Mozilla Firefox či e-mailový klient Mozilla Thunderbird. Na rozdíl od Mozilla Foundation se jedná o komerční společnost, která produkuje zisk. Nejedná se však o primární motivaci, protože veškerý příjem putuje zpět do projektů souvisejících s Mozillou.</p>
            </div>
          </div>
        </div>
      </div>


      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="https://upload.wikimedia.org/wikipedia/en/thumb/6/6c/RedHat.svg/1280px-RedHat.svg.png">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Společnost Red Hat v České republice působí již od roku 2004 a už od té doby spolupracuje s FIT VUT<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="https://www.redhat.com/en/global/czech-republic">Red Hat</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
              <span class="card-title">Red Hat<i class="material-icons right">close</i></span>
              <p>Společnost Red Hat v České republice působí již od roku 2004 a už od té doby spolupracuje s FIT VUT.Pobočka se soustředí jak na testování, tak i vývoj softwaru. Koncem roku 2012 měl Red Hat v Brně 500 zaměstnanců, v roce 2014 pak přes 700, což dělá brněnskou pobočku největším světovým střediskem společnosti hned za hlavním sídlem.</p>
            </div>
          </div>
        </div>
      </div>
    </div>





    <div class="row">
      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="http://design.ubuntu.com/wp-content/uploads/logo-ubuntu_st_no%C2%AE-orange-hex.png">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Ubuntu je komunitně vyvíjený operační systém - zastřešený britskou společností Canonical, vhodný pro laptopy, stolní počítače i servery<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="http://www.ubuntu.cz/">Ubuntu</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
            <span class="card-title">Ubuntu<i class="material-icons right">close</i></span>
              <p>Ubuntu je komunitně vyvíjený operační systém - zastřešený britskou společností Canonical, vhodný pro laptopy, stolní počítače i servery. Ubuntu obsahuje všechny aplikace, které budete pro běžnou práci potřebovat: od textového procesoru a e-mailového klienta přes řadu her až po nejrůznější nástroje. Ubuntu je plně přeloženo do 25 jazyků, včetně češtiny.</p>
              <p>Ubuntu je a bude zdarma. Nemusíte platit licenční poplatky, ani vyplňovat složité licenční klíče. Můžete jej legálně stáhnout, používat a sdílet.</p>
            </div>
          </div>
        </div>
      </div>


      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="https://www.it.ox.ac.uk/sites/dandy/files/images/event/Lenovo%20Logo%20large.png">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Lenovo Group Limited je největší světový výrobce počítačů – nadnárodní firma, která kromě výroby počítačů a periferií vyvíjí i související technologie.<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="http://www.lenovo.com/cz/cs/">LENOVO</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
            <span class="card-title">Lenovo<i class="material-icons right">close</i></span>
              <p>Původně (od roku 1988, kdy se stala korporací) působila pod značkou Legend, později byl její název změněn na portmonteau „nová legenda“ (latinsky Legend novo). V roce 2005 firma koupila počítačovou divizi IBM a zařadila se mezi největší výrobce počítačů na světě. V lednu 2014 souhlasilo Lenovo s převzetím divize mobilních zařízení firmy Motorola Mobility od firmy Google.</p>
            </div>
          </div>
        </div>
      </div>


      <div class="col s4">
        <div class="card">

          <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Intel-logo.svg/2000px-Intel-logo.svg.png">
            <span class="card-title">&nbsp;</span>
          </div>

          <div class="card-content" style="text-align: justify;">
            <p>Společnost Intel Corporation je největším světovým výrobcem polovodičových obvodů, procesorů a dalších zařízení<i class="card-title material-icons right activator">more_vert</i></p>
          </div>

          <div class="card-action">
            <a href="http://www.intel.eu/content/www/eu/en/homepage.html">Intel</a>
          </div>

          <div class="card-reveal">
            <div class="card-content" style="text-align: justify;">
            <span class="card-title">IBM<i class="material-icons right">close</i></span>
            <p>Společnost Intel Corporation je největším světovým výrobcem polovodičových obvodů a dalších zařízení. Hlavní sídlo firmy je v kalifornském městě Santa Clara v USA, v lokalitě nazývané Silicon Valley. Celé sídlo Intelu je složeno z komplexu několika budov, centrála je pak v budově pojmenované po zakladateli společnosti Robertu Noyceovi.</p>
            <p>Intel je znám mezi běžnými lidmi především svými procesory. Ročně jich vyrobí přes 100 miliónů, což je zhruba 75 % celosvětové produkce x86 procesorů.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}