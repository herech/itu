<?php
// source: /www/sites/0/site23020/public_html/nette-blog/app/VerejnyModule/presenters/templates/Homepage/default.latte

class Templatec57606be79a423318e83f1c896cc0e68 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('bbfbaa9ae0', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb6190c08627_content')) { function _lb6190c08627_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>        
    <div class="row">
    </div>
    <div class="container">
        <div class="row">

        <div class="col s12 left-align">
            <span class="flow-text">FIT VUT v Brně nabízí firmám spolupráci na následujících typech projektů:</span>
        </div>
    
    </div>
    </div>

  <div class="slider">
    <ul class="slides">
      <li style="background:#707AC3;">
      <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:drobne"), ENT_COMPAT) ?>
">
        <div class="caption left-align">
          <h3>Drobné projekty</h3>
          <h5 class="light grey-text text-lighten-3">Projekty vhodné do předmětů s rozsahem 1-3 měsíce, nízká finanční náročnost, vhodné pro drobné testování a menší problémy, IP firmy</h5>
        </div>
      </a>
      </li>  
      <li style="background:#70C3BC;">
      <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:stredni"), ENT_COMPAT) ?>
">
        <div class="caption right-align">
          <h3>Střední projekty</h3>
          <h5 class="light grey-text text-lighten-3">Projekty vhodné jako diplomová práce, delší doba řešení (6-8 měsíců), nízká finanční náročnost, vhodné pro zkoumání rizikových nápadů nebo vývoje základních technologií, IP dle dohody.</h5>
        </div>
      </a>
      </li>
      <li style="background:#81C370;">
      <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:vetsi"), ENT_COMPAT) ?>
">
        <div class="caption center-align">
          <h3>Větší projekty</h3>
          <h5 class="light grey-text text-lighten-3">Projekty vhodné pro vývoj složitějších řešení, vyšší finanční náročnost, časová zátěž dle domluvy, forma smluvního výzkumu, IP dle domluvy.</h5>
        </div>
      </a>
      </li>
      <li style="background:#C370A6;">
      <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:czEu"), ENT_COMPAT) ?>
">
        <div class="caption left-align">
          <h3>Tuzemské i Evropské projekty</h3>
          <h5 class="light grey-text text-lighten-3">Spolupráce na tuzemských i Evropských projektech</h5>
        </div>
      </a>
      </li> 
      <li class="orange lighten-1">
      <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link(":Verejny:Spoluprace:sponzoring"), ENT_COMPAT) ?>
">
        <div class="caption right-align">
          <h3>Sponzoring akcí</h3>
          <h5 class="light grey-text text-lighten-3">Sponzoring akcí pořádaných FIT, další typy sponzoringu.</h5>
        </div>
      </a>
      </li>
      
    </ul>
  </div>
  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
  
  <div class="container">
    <div class="row">
        
        <div class="col s6">
                <center><span class="flow-text">Zajímavé výsledky</span></center>
                
                <div class="card-panel blue-grey" style="text-align: justify;">
                  <span class="white-text">Projekt <em>Certifikace CMMI ve vývoji software v agilním prostředí</em> který byl byl zadán firmou Seemens a vypracován studentem FIT VUT získal cenu <em>Wernera von Seemens 2014.</em>
                  </span>
                </div>
        </div>
        
        <div class="col s6">
                <center><span class="flow-text">&nbsp;</span></center>
    
                <div class="card-panel blue-grey" style="text-align: justify;">
                  <span class="white-text">V rámci úspěšně vypracovaného projektu <em>Multifunkční kamerový systém s automatizovaným zpracováním videodat</em> získala v roce 2015 skupina vědců z FIT VUT prestižní cenu Akademie věd ČR za dosažené vynikající výsledky velkého vědeckého významu.
                  </span>
                </div>
        </div>
        
    <div class="row">
    </div>
        <div class="col s6">
                <center><span class="flow-text">Co se o nás píše</span></center>
   
                <div class="card-panel light-blue accent-3" style="text-align: justify;">
                  <span class="white-text">Red Head, Inc.<blockquote style="border-left:5px solid #0277bd"><em>Dlouho jsme hledali levnou, ale odbornou pracovní sílu, pro řešení našich méně prioritních projektů. Nejdříve jsem oslovili Fakultu informatiky Masarykovy univerzity, ale po několika neúspěšných projektech jsme se rozhodli hledat další lidské zdroje. Narazili jsme na tento skvělý portál pro spolupráci studetů a Fakulty informačních technologií VUT a rozhodli jsme se vyzkoušet nabízenou spolupráci. Všechny vypsané projekty byly úspěšně v termínu vyřešeny a naše spolupráce nadále pokračuje.</em></blockquote>
                  </span>
                </div>
        </div>
        
        <div class="col s6">
                <center><span class="flow-text">&nbsp;</span></center>

                <div class="card-panel light-blue accent-3" style="text-align: justify;">
                  <span class="white-text">H2O Czech Republic a.s.<blockquote style="border-left:5px solid #0277bd"><em>V rámci našeho nového projektu jsme si nechali zpracovat u FIT VUT výzkum v oblasti bezpečnosti a monitorování počítačových sítí. S výsledky výzkumu jsme byli spokojeni a plánujeme při další příližitosti opět spolupracovat s FIT VUT prostřednitvím tohoto portálu.</em></blockquote>
                  </span>
                </div>
        </div>
    </div>
  </div>
  
  <div class="row"></div>
  <div class="row"></div>
  <div class="row"></div>
  
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIRuntime::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>






<?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}