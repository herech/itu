<?php
namespace App\Model;

use Nette\Security as NS;
use Nette;

class MyAuthenticator extends Nette\Object implements NS\IAuthenticator
{
    public $database;

    function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('spravce')
            ->where('login', $username)->fetch();

		if ($row) {

			if (!NS\Passwords::verify($password, $row->heslo)) {
				throw new NS\AuthenticationException('Špatné heslo.');
			}
		
			$role = "Spravce"; 

			return new NS\Identity($row->id_spravce, explode(" ",$role), array('username' => $row->login));
        }
		
		$row = $this->database->table('student')
            ->where('login', $username)->fetch();

		if ($row) {

			if (!NS\Passwords::verify($password, $row->heslo)) {
				throw new NS\AuthenticationException('Špatné heslo.');
			}
		
			$role = "Student"; 

			return new NS\Identity($row->id_studenta, explode(" ",$role), array('username' => $row->login));
        }
		
		$row = $this->database->table('akademik')
            ->where('login', $username)->fetch();

		if ($row) {

			if (!NS\Passwords::verify($password, $row->heslo)) {
				throw new NS\AuthenticationException('Špatné heslo.');
			}
		
			$role = "Akademik"; 

			return new NS\Identity($row->id_akademika, explode(" ",$role), array('username' => $row->login));
        }
		
		$row = $this->database->table('firma')
            ->where('login', $username)->fetch();

		if ($row) {

			if (!NS\Passwords::verify($password, $row->heslo)) {
				throw new NS\AuthenticationException('Špatné heslo.');
			}
		
			$role = "Firma"; 

			return new NS\Identity($row->id_firmy, explode(" ",$role), array('username' => $row->login));
        }
		
		
        throw new NS\AuthenticationException('Uživatel s daným loginem neexistuje.');

    }
}
