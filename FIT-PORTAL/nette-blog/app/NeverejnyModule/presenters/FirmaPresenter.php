<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class FirmaPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->firmy = $this->database->table('firma')->order('login');
    }

    public function actionCreate()
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění přidávat firmu', 'alert-box alert');
            $this->redirect('default');
        }
        $this['firmaForm']['rozsahy']->setDisabled();
    }

    public function actionRegister()
    {
        if(!$this->getUser()->isInRole('Firma')) {
            $this->flashMessage('Nemáte oprávnění zastřešit projekt', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionUnregister()
    {
        if(!$this->getUser()->isInRole('Firma')) {
            $this->flashMessage('Nemáte oprávnění zrušit zastřešení projektu', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionEdit($id_firmy)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Firma')) {
            $this->flashMessage('Nemáte oprávnění upravovat firmu', 'alert-box alert');
            $this->redirect('default');
        }

        if($this->getUser()->isInRole('Firma') && $id_firmy != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění upravovat jinou firmu', 'alert-box alert');
            $this->redirect('default');
        }

        $firma = $this->database->table('firma')->get($id_firmy);

        if (!$firma) {
            $this->flashMessage('Firma nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }

        if ($this->getUser()->isInRole('Firma')) {
            $this['firmaForm']['id_akademika']->setItems(array($firma->id_akademika => $firma->akademik->login));
        }

        $reg_roz = $this->database->table('firma_rozsah')->where('id_firmy', $id_firmy);
        $registrovane_rozsahy = array();
        foreach ($reg_roz as $reg_rozz) {
            $registrovane_rozsahy[] = $reg_rozz->id_rozsahu_projektu;
        }

        $this['firmaForm']['rozsahy']->setDefaultValue($registrovane_rozsahy);

        $this['firmaForm']->setDefaults($firma->toArray());

        if ($this->getUser()->isInRole('Firma')) {
            $this['firmaForm']['id_akademika']->setDisabled();
            $this['firmaForm']['login']->setAttribute('placeholder', $firma->login);
            $this['firmaForm']['login']->setDisabled();
        }
    }

    protected function createComponentFirmaForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('login', 'login')
        ->setRequired('Prosím vyplňte svůj login')
        ->setAttribute('placeholder', 'Váš login');

        $form->addText('email', 'email')
        ->setRequired('Prosím vyplňte svůj email')
        ->setAttribute('placeholder', 'Váš email');

        $form->addPassword('heslo', 'Heslo')
        ->setAttribute('placeholder', 'Vaše heslo (alespoň pětimístné)')
        ->addCondition(Form::FILLED)
        ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň 5 znaků', 5);

        $form->addPassword('heslo_overeni', 'Heslo pro kontrolu')
        ->setAttribute('placeholder', 'Zadejte heslo znovu pro ověření')
        ->setOmitted(TRUE)
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['heslo']);

        $akademici = $this->database->table('akademik')->order('login');
        $arrAkademiku = array();
        foreach ($akademici as $akademik) {
            $arrAkademiku[$akademik->id_akademika] = $akademik->login;
        }
        $form->addSelect('id_akademika', 'Akademik', $arrAkademiku);

        $rozsahy = $this->database->table('rozsah_projektu');
        $arrRozsahu = array();
        foreach ($rozsahy as $rozsah) {
            $arrRozsahu[$rozsah->id_rozsah_projektu] = ' '.$rozsah->rozsah;
        }
        $form->addCheckboxList('rozsahy', 'Rozsah projektu', $arrRozsahu);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'firmaFormSucceeded');

        return $form;
    }

    protected function createComponentRegistraceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('id_firmy', $this->user->id);

            $tmpSelection = $this->database->table('projekt')->where('typ', 'studentský')->where('stav', 'aktivní');
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('firma_zastresovatel')
                    ->where('id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->id_firmy) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        $volneProjekty = $this->database->table('projekt')->where('id_projektu', $seletion);
        $arrProjektu = array();
        foreach ($volneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Zastřešit projekt')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'registraceFormSucceeded');

        return $form;
    }

    protected function createComponentOdregistraceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('id_firmy', $this->user->id);

            $zastresene = $this->database->table('firma_zastresovatel')->where('id_firmy', $this->user->id);
            $seletion = array();
            foreach($zastresene as $tmp) {
                $seletion[] = $tmp->id_projektu;
            }

        $volneProjekty = $this->database->table('projekt')->where('id_projektu', $seletion);
        $arrProjektu = array();
        foreach ($volneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Odzastřešit projekt')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'odregistraceFormSucceeded');

        return $form;
    }

    public function firmaFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Firma')) {
            $this->flashMessage('Nemáte oprávnění upravovat firmu', 'alert-box alert');
            $this->redirect('default');
        }

        $doslo_k_chybe = false;
        $id_firmy = $this->getParameter('id_firmy');

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        if ($id_firmy) {
            if($this->getUser()->isInRole('Firma') && $id_firmy != $this->user->id) {
                $this->flashMessage('Nemáte oprávnění upravovat jinou firmu', 'alert-box alert');
                $this->redirect('default');
            }

            if (isset($values['heslo'])) {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
            }


            $zvolene_rozsahy = $values['rozsahy'];
            unset($values['rozsahy']);


            $reg_roz = $this->database->table('firma_rozsah')->where('id_firmy', $id_firmy);
            $registrovane_rozsahy = array();
            foreach ($reg_roz as $reg_rozz) {
                $registrovane_rozsahy[] = $reg_rozz->id_rozsahu_projektu;
            }


            $pridat = array_diff($zvolene_rozsahy, $registrovane_rozsahy);
            $smazat = array_diff($registrovane_rozsahy, $zvolene_rozsahy);


            foreach ($pridat as $id_rozsahu) {
                $rozsah = array('id_firmy' => $id_firmy, 'id_rozsahu_projektu' => $id_rozsahu);
                $this->database->table('firma_rozsah')->insert($rozsah);
            }


            foreach ($smazat as $id_rozsahu) {
                $rozsah = $this->database->table('firma_rozsah')->where('id_firmy', $id_firmy)->where('id_rozsahu_projektu', $id_rozsahu);
                $rozsah->delete();
            }


            $firma = $this->database->table('firma')->get($id_firmy);
            $firma->update($values);
        } else {
            if(!$this->getUser()->isInRole('Spravce')) {
                $this->flashMessage('Nemáte oprávnění přidávat firmu', 'alert-box alert');
                $this->redirect('default');
            }

            if (empty($values['heslo'])) {
                $form->addError('Zadejte prosím své heslo');
                $doslo_k_chybe = true;
            }
            else {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                $firma = $this->database->table('firma')->insert($values);
            }
        }

        if ( $doslo_k_chybe == false) {
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');

            $this->redirect('default');
        }
    }

    public function registraceFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Firma') || $values['id_firmy'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění zastřešit projekt', 'alert-box alert');
            $this->redirect('default');
        }

            $tmpSelection = $this->database->table('projekt')->where('typ', 'studentský')->where('stav', 'aktivní');
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('firma_zastresovatel')
                    ->where('id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->id_firmy) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        if (in_array($values->id_projektu, $seletion)) {
            $this->database->table('firma_zastresovatel')->insert($values);
        }
        else {
            $this->flashMessage('Nelze vložit zastřešovatele', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    public function odregistraceFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Firma') || $values['id_firmy'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění zrušit zastřešení projektu', 'alert-box alert');
            $this->redirect('default');
        }

        $firma_zastresovatel = $this->database->table('firma_zastresovatel')
            ->where('id_firmy', $values['id_firmy'])->where('id_projektu', $values['id_projektu'])->fetch();

        if (!$firma_zastresovatel) {
            $this->flashMessage('Firma jako zastřešovatel nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $firma_zastresovatel->delete();
            $this->flashMessage('Firma jako zastřešovatel byla smazána', 'alert-box success');
            $this->redirect('default');
        }
    }

    public function actionDelete($id_firmy)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění mazat firmu', 'alert-box alert');
            $this->redirect('default');
        }

        $firma = $this->database->table('firma')->get($id_firmy);

        if (!$firma) {
            $this->flashMessage('Firma nebyla nalezena', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $firma->delete();
            $this->flashMessage('Firma byla smazána', 'alert-box success');
            $this->redirect('default');
        }
    }
}
