<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use Nette\Security;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;


class ProjektPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }
	
	public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->projekty = $this->database->table('projekt')->order('nazev');

        $this->template->garantovatProjekt = false;
        $this->template->registrovatProjekt = false;
        $this->template->zastresitProjekt = false;

        if ($this->getUser()->isInRole('Akademik')) {
            $this->template->garantovatProjekt = true;
        }
        else if ($this->getUser()->isInRole('Student')) {
            $this->template->registrovatProjekt = true;
        }
        else if ($this->getUser()->isInRole('Firma')) {
            $this->template->zastresitProjekt = true;
        }
    }

    public function renderDetail($id_projektu) {
	
		if ($id_projektu) {
			$projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();
			if (!$projekt) {
				$this->flashMessage('Chyba: Projekt nebyl nalezen', 'alert-box alert');
				$this->redirect('default');
			}
            
            $this->template->projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();
            
            $this->template->projekt_resitel = $this->database->table('student_resitel')->where('projekt_id_projektu = ?',$id_projektu);
            $this->template->projekt_stitky = $this->database->table('projekt_stitky')->where('id_projektu = ?',$id_projektu);
            
            $this->template->hodnoceni_komentare = $this->database->table('hodnoceni')->where('id_projektu = ?', $id_projektu);
            
			$this->template->hodnoceni = false;
            $this->template->nahrani_zpravy = false;
            
            $projekt_resitel = $this->database->table('student_resitel')->where('projekt_id_projektu = ? AND student_id_studenta = ?',$id_projektu, $this->getUser()->getIdentity()->getId())->fetch();
            if ($projekt_resitel) {
                $this->template->nahrani_zpravy = true;
            }
			
			if ($projekt->stav == "dokončený") {
				
				$role = $this->getUser()->getRoles();

				switch($role[0]) {
					case "Student":
						$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_studenta = ?', $projekt->id_zadavatele, "student", $this->getUser()->getIdentity()->getId())->fetch();
						if (!$row) {
							$this->template->hodnoceni = true;						
						}
						else {
							$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
							if (!$row) {
								$this->template->hodnoceni = true;
							}
						}
						break;
					case "Firma":
						$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_firmy = ?', $projekt->id_zadavatele, "firma", $this->getUser()->getIdentity()->getId())->fetch();
						if (!$row) {
							$this->template->hodnoceni = true;						
						}
						else {
							$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
							if (!$row) {
								$this->template->hodnoceni = true;
							}
						}
						break;
					case "Akademik":
						$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_akademika = ?', $projekt->id_zadavatele, "akademik", $this->getUser()->getIdentity()->getId())->fetch();
						if (!$row) {
							$this->template->hodnoceni = true;						
						}
						else {
							$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
							if (!$row) {
								$this->template->hodnoceni = true;
							}
						}
						break;
				}
			}	
		}
		else {
			$this->flashMessage('Chyba: Projekt nebyl nalezen', 'alert-box alert');
			$this->redirect('default');
		}
	}
    
	
	public function actionEdit($id_projektu)
    {
        $projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();


        if (!$projekt) {
            $this->flashMessage('Chyba: Projekt nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        
        if ($projekt->stav != "aktivní") {
			$this->flashMessage('Chyba: Projekt je buď zrušený nebo dokončený, nelze jej upravovat', 'alert-box alert');
            $this->redirect('default');
		}
        
        $zadavatel_hodnotitel = $projekt->ref('zadavatel_hodnotitel', 'id_zadavatele');
		switch ($zadavatel_hodnotitel->typ) {
			case "firma":
				if ($zadavatel_hodnotitel->id_firmy != $this->getUser()->getIdentity()->getId()) {
					$this->flashMessage('Chyba: Nemáte oprávnění editovat projekt', 'alert-box alert');
					$this->redirect('default');
				}
				break;
			case "student":
				if ($zadavatel_hodnotitel->id_studenta != $this->getUser()->getIdentity()->getId()) {
					$this->flashMessage('Chyba: Nemáte oprávnění editovat projekt', 'alert-box alert');
					$this->redirect('default');
				}
				break;
		}

        $this['pridejNovyProjektForm']->setDefaults($projekt->toArray());
        $projekty_stitky = $this->database->table('projekt_stitky')->where('id_projektu = ?', $id_projektu);    
        $seznam_stitku = "";
        foreach ($projekty_stitky as $projekt_stitek) {
            $seznam_stitku .= $projekt_stitek->ref('stitky', 'id_stitku')->nazev . ",";
        }
        $seznam_stitku = substr($seznam_stitku, 0, -1);
        
        $this['pridejNovyProjektForm']['stitky']->setDefaultValue($seznam_stitku); 
    }
    
    public function actionDokoncitProjekt($id_projektu)
    {
        $projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();


        if (!$projekt) {
            $this->flashMessage('Chyba: Projekt nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        
        if ($projekt->stav != "aktivní") {
			$this->flashMessage('Chyba: Projekt je buď zrušený nebo dokončený, nelze jej dokončit', 'alert-box alert');
            $this->redirect('default');
		}
        
        // pokud chce akademik dokončit projekt který vede
        if ($projekt->id_akademika && $projekt->ref('akademik', 'id_akademika')->id_akademika == $this->getUser()->getIdentity()->getId()) {
			$projekt->update(array("stav" => "dokončený"));
            $this->flashMessage('Projekt byl dokončen', 'alert-box success');
		}
        else {
            $this->flashMessage('Projekt nemůžete dokončit, dokončit projekt může pouze akademik, který jej vede', 'alert-box alert');
        }
		$this->redirect('default');
    }
	
	public function actionZrusit($id_projektu)
    {
        $projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();

        if (!$projekt) {
            $this->flashMessage('Chyba: Projekt nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        
        if ($projekt->stav != "aktivní") {
			$this->flashMessage('Chyba: Projekt je buď zrušený nebo dokončený, nelze jej zrušit', 'alert-box alert');
            $this->redirect('default');
		}
        
        // pokud chce akademik smazat projekt který vede
        if ($projekt->id_akademika && $projekt->ref('akademik', 'id_akademika')->id_akademika == $this->getUser()->getIdentity()->getId()) {
			$projekt->update(array("stav" => "zrušený"));
		}
		// pokud chce firma nebo student smazat své téma
		else {
			$zadavatel_hodnotitel = $projekt->ref('zadavatel_hodnotitel', 'id_zadavatele');
			switch ($zadavatel_hodnotitel->typ) {
				case "firma":
					if ($zadavatel_hodnotitel->id_firmy != $this->getUser()->getIdentity()->getId()) {
						$this->flashMessage('Chyba: Nemáte oprávnění mazat projekt', 'alert-box alert');
						$this->redirect('default');
					}
					break;
				case "student":
					if ($zadavatel_hodnotitel->id_studenta != $this->getUser()->getIdentity()->getId()) {
						$this->flashMessage('Chyba: Nemáte oprávnění mazat projekt', 'alert-box alert');
						$this->redirect('default');
					}
					break;
			}
		}
		
		$projekt->update(array("stav" => "zrušený"));
		
		$this->flashMessage('Projekt byl zrušen', 'alert-box success');
		$this->redirect('default');
    }
	
	protected function createComponentPridejNovyProjektForm()
    {
        $form = new Form();

        $form->addText('nazev', 'Název projektu')
        ->setAttribute('placeholder', 'Název projektu')
		->setAttribute('maxlength', 100)
		->addRule(Form::MAX_LENGTH, 'Název může mít délků max. %d znaků', 100)
		->setRequired('Prosím vyplňte název projektu');
        
        $form->addTextarea('popis_zadani', 'Popis zadání')
        ->setAttribute('placeholder', 'Popis zadání')
		->setRequired('Prosím vyplňte popis zadání projektu');
		
		$form->addTextarea('popis_pozadovanych_vysledku', 'Popis požadovaných výsledků')
        ->setAttribute('placeholder', 'Popis požadovaných výsledků');
		
		$form->addTextarea('popis_dosazenych_vysledku', 'Popis dosažených výsledků')
        ->setAttribute('placeholder', 'Popis dosažených výsledků');
		
		$form->addRadioList('verejny', 'Veřejný projekt:', array(
			1 => 'Ano',
			0 => 'Ne',
		));
		
		$form['verejny']->setDefaultValue(1);
		
		$form->addText('max_pocet_prihlasenych', 'Maximální počet přihlášených')
        ->setAttribute('placeholder', 'Maximální počet přihlášených')
        ->addRule(Form::INTEGER, 'Počet přihlášených musí být celé kladné číslo')
        ->addRule(Form::RANGE, 'Počet přihlášených musí být celé kladné číslo', array(0, null));
        
		$rozsahy_projektu = $this->database->table('rozsah_projektu');
		
		$rozsahy = array();
		
		foreach ($rozsahy_projektu as $rozsah) {
			$rozsahy[$rozsah->id_rozsah_projektu] = $rozsah->rozsah;
		}
		
        $form->addSelect('id_rozsahu_projektu', 'Rozsah projektu', $rozsahy);
        
        $form->addText('stitky', 'Štítky pro daný projekt(seznam štítků oddelených čárkami)')
        ->setAttribute('placeholder', 'Zadejte seznam štítků oddelených čárkou');
		
        $form->addSubmit('send', 'Vložit projekt do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejNovyProjektFormSucceeded');

        return $form;
    }


    public function pridejNovyProjektFormSucceeded($form, $values)
    {    

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);
        
        $id_projektu = $this->getParameter('id_projektu');
        
        $doslo_k_chybe = false;
        
        try {
        
			// pokud se jedná o editaci projektu
            if ($id_projektu) {
                $projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();
                if (!$projekt) {
					throw new \Exception();
				}
				
				if ($projekt->stav != "aktivní") {
					throw new \Exception();
				}
				
				$zadavatel_hodnotitel = $projekt->ref('zadavatel_hodnotitel', 'id_zadavatele');
				switch ($zadavatel_hodnotitel->typ) {
					case "firma":
						if ($zadavatel_hodnotitel->id_firmy != $this->getUser()->getIdentity()->getId()) {
							throw new \Exception();
						}
						break;
					case "student":
						if ($zadavatel_hodnotitel->id_studenta != $this->getUser()->getIdentity()->getId()) {
							throw new \Exception();
						}
						break;
				}
				
                
                if (!isset($values->stitky)) {
                    $poleStitku = array();
                }
                else {
                    $poleStitku = explode(',', $values->stitky);
                }
                $nektery_stitek_nenalezen = array();
            
                // zjistíme jestli se nějaký štítek už v db vyskytuje
                foreach ($poleStitku as $stitek) {
                
                    if ($stitek == '') continue;
                    if (!$this->database->table('stitky')->where('nazev = ?', $stitek)->fetch()) {
                        $nektery_stitek_nenalezen[] = $stitek;
                    }
                }
                // pokud alespoň jeden štítek nebyl v db nalezen
                if (!empty($nektery_stitek_neulozen)) {
                    $form->addError('Dané štítky nejsou povoleny a tak je nelze použít: ' . implode(", ", $nektery_stitek_neulozen));
                    $doslo_k_chybe = true;
                }
                
                if ($doslo_k_chybe == false) {
                    unset($values['stitky']);
                    $projekt->update($values);
                
                    $projekty_stitky = $this->database->table('projekt_stitky')->where('id_projektu = ?', $id_projektu)->delete();                   
                    
                    foreach ($poleStitku as $stitek) {
                        if ($stitek == '') continue;
                        $id_stitku = $this->database->table('stitky')->where('nazev = ?', $stitek)->fetch()->id_stitku;
                        $this->database->table('projekt_stitky')->insert(array('id_stitku' => $id_stitku, 'id_projektu' => $id_projektu));
                    }
                 
                }
            }
            
            // pokud se jedná o vložení projketu
            else {
				$role = $this->getUser()->getRoles();

				switch($role[0]) {
					case "Student":
						$row = $this->database->table('zadavatel_hodnotitel')->insert(array("typ" => "student", "id_studenta" => $this->getUser()->getIdentity()->getId()));
						$values["typ"] = "studentský";
						break;
					case "Firma":
						$row = $this->database->table('zadavatel_hodnotitel')->insert(array("typ" => "firma", "id_firmy" => $this->getUser()->getIdentity()->getId()));
						$values["typ"] = "firemní";
						break;
					default:
						throw new \Exception();
						break;
				}
                
                if (!isset($values->stitky)) {
                    $poleStitku = array();
                }
                else {
                    $poleStitku = explode(',', $values->stitky);
                }
                $nektery_stitek_nenalezen = array();
            
                // zjistíme jestli se nějaký štítek už v db vyskytuje
                foreach ($poleStitku as $stitek) {
                
                    if ($stitek == '') continue;
                    if (!$this->database->table('stitky')->where('nazev = ?', $stitek)->fetch()) {
                        $nektery_stitek_nenalezen[] = $stitek;
                    }
                }
                // pokud alespoň jeden štítek nebyl v db nalezen
                if (!empty($nektery_stitek_nenalezen)) {
                    $form->addError('Dané štítky nejsou povoleny a tak je nelze použít: ' . implode(", ", $nektery_stitek_nenalezen));
                    $doslo_k_chybe = true;
                }
                
                if ($doslo_k_chybe == false) {
                    $values["id_zadavatele"] = $row->id_zadavatele_hodnotitele;
                    unset($values['stitky']);
                    $id = $this->database->table('projekt')->insert($values);
                
                    $projekty_stitky = $this->database->table('projekt_stitky')->where('id_projektu = ?', $id->id_projektu)->delete();                   
                    
                    foreach ($poleStitku as $stitek) {
                        if ($stitek == '') continue;
                        $id_stitku = $this->database->table('stitky')->where('nazev = ?', $stitek)->fetch()->id_stitku;
                        
                        $this->database->table('projekt_stitky')->insert(array('id_stitku' => $id_stitku, 'id_projektu' => $id->id_projektu));
                    }
                 
                }
                
            }
            
            if ($doslo_k_chybe == false) {   
                $this->flashMessage('Projekt byl vytvořen/upraven', 'alert-box success');
                $this->redirect('default');
            }
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            
            $this->flashMessage('Chyba: Nový projekt nemohl být vytvořen/upraven' . $e->getMessage(), 'alert-box alert');
            $this->redirect('default');
        }
    }
    
    protected function createComponentPridejTechnickouZpravuForm()
    {
        $form = new Form();

        $form->addUpload('technicka_zprava', 'Přidejte technickou zprávu')
        ->setAttribute('placeholder', 'Pouze pdf soubory jsou přípustné')
        ->addRule(Form::MIME_TYPE, 'Pouze pdf soubory jsou přípustné', 'application/pdf')
        ->addRule(Form::MAX_FILE_SIZE, 'Max size is 3 Mb.', 3000 * 1024);
     
		
        $form->addSubmit('send', 'Uložit technickou zprávu')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejTechnickouZpravuFormSucceeded');

        return $form;
    }
    
    public function pridejTechnickouZpravuFormSucceeded($form) {
        if ($this->getUser()->isInRole('Student') && $this->getParameter('id_projektu')) {
        
            $id_projektu = $this->getParameter('id_projektu');
            $projekt_resitel = $this->database->table('student_resitel')->where('projekt_id_projektu = ? AND student_id_studenta = ?',$id_projektu, $this->getUser()->getIdentity()->getId())->fetch();
            
            if ($projekt_resitel) {
                $file = $form['technicka_zprava']->getValue();
                $name = $file->getName();
                $id = $this->getUser()->getIdentity()->getId();
                $date = new DateTime();
                $upload = WWW_DIR . "/images/" . $name . $id . $date . ".pdf";

                if ($file->isOK()){
                    $file->move($upload);
                }

                $projekt_resitel->update(array(
                        'cesta_k_technicke_zprave' => $name. $id . $date. ".pdf",
                        'student_id_studenta' => $id
                    ));


               $this->flashMessage('Technická zpráva byla úspěšně nahrána.', 'alert-box success');
               $this->redirect('this');
           }
       }

          $this->flashMessage('Technickou zprávu se nepodařilo nahrát, kvůli nedostatečným oprávněním.', 'alert-box success');
          $this->redirect('this');
    }
    
    protected function createComponentPridejHodnoceniForm()
    {
        $form = new Form();

        $form->addText('komentar', 'Komentář k hodnocení')
        ->setAttribute('placeholder', 'Komentář k hodnocení');
        
        $form->addHidden('znamka');
		
        $form->addSubmit('send', 'Uložit hodnocení')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridejHodnoceniFormSucceeded');

        return $form;
    }


    public function pridejHodnoceniFormSucceeded($form, array $values)
    {    

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);
        
        $id_projektu = $this->getParameter('id_projektu');
        
        try {
        
			
            if ($id_projektu) {
                $projekt = $this->database->table('projekt')->where('id_projektu = ?',$id_projektu)->fetch();
                if (!$projekt) {
					throw new \Exception();
				}
				
				if ($projekt->stav != "dokončený") {
					throw new \Exception();
				}
				
				$role = $this->getUser()->getRoles();

				switch($role[0]) {
					case "Student":
						$student_resitel = $this->database->table('student_resitel')->where('projekt_id_projektu = ? AND student_id_studenta = ?', $id_projektu, $this->getUser()->getIdentity()->getId())->fetch();
						if (!$student_resitel) {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_studenta = ?', $projekt->id_zadavatele, "student", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								throw new \Exception();
							}
						}
						else {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_studenta = ?', $projekt->id_zadavatele, "student", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								$row = $this->database->table('zadavatel_hodnotitel')->insert(array("typ" => "student", "id_studenta" => $this->getUser()->getIdentity()->getId()));
							}
						}
						$values["id_projektu"] = $id_projektu;
						$values["id_zadavatele_hodnotitele"] = $row->id_zadavatele_hodnotitele;
						
						$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
						if ($row) {
							throw new \Exception();
						}
						
						$this->database->table('hodnoceni')->insert($values);
						break;
					case "Firma":
						$firma_zastresovatel = $this->database->table('firma_zastresovatel')->where('id_projektu = ? AND id_firmy = ?', $id_projektu, $this->getUser()->getIdentity()->getId())->fetch();
						if (!$firma_zastresovatel) {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_firmy = ?', $projekt->id_zadavatele, "firma", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								throw new \Exception();
							}
						}
						else {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_firmy = ?', $projekt->id_zadavatele, "firma", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								$row = $this->database->table('zadavatel_hodnotitel')->insert(array("typ" => "firma", "id_firmy" => $this->getUser()->getIdentity()->getId()));
							}
						}
						$values["id_projektu"] = $id_projektu;
						$values["id_zadavatele_hodnotitele"] = $row->id_zadavatele_hodnotitele;
												
						$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
						if ($row) {
							throw new \Exception();
						}
						
						$this->database->table('hodnoceni')->insert($values);
						break;
					case "Akademik":
						$akademik = $projekt->ref('akademik', 'id_akademika');
						if (!$akademik || $akademik->id_akademika != $this->getUser()->getIdentity()->getId()) {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_akademika = ?', $projekt->id_zadavatele, "akademik", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								throw new \Exception();
							}
						}
						else {
							$row = $this->database->table('zadavatel_hodnotitel')->where('id_zadavatele_hodnotitele = ? AND typ = ? AND id_akademika = ?', $projekt->id_zadavatele, "akademik", $this->getUser()->getIdentity()->getId())->fetch();
							if (!$row) {
								$row = $this->database->table('zadavatel_hodnotitel')->insert(array("typ" => "akademik", "id_akademika" => $this->getUser()->getIdentity()->getId()));
							}
						}
						$values["id_projektu"] = $id_projektu;
						$values["id_zadavatele_hodnotitele"] = $row->id_zadavatele_hodnotitele;
												
						$row = $this->database->table('hodnoceni')->where('id_projektu = ? AND id_zadavatele_hodnotitele = ?', $id_projektu, $row->id_zadavatele_hodnotitele)->fetch();
						if ($row) {
							throw new \Exception();
						}
						
						$this->database->table('hodnoceni')->insert($values);
						break;
					default:
						throw new \Exception();
						break;
				}
				
            }
        
            $this->flashMessage('Hodnocení spolupráce bylo přidáno', 'alert-box success');
            $this->redirect('this');
        }
        catch (\Exception $e) {
            if ($e instanceof \Nette\Application\AbortException) {
                throw $e;
            }
            $this->flashMessage('Chyba: Hodnocení nemohlo být přidáno', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function handleGarantovat($id_projektu)
    {
        if(!$this->getUser()->isInRole('Akademik')) {
            $this->flashMessage('Nemáte oprávnění garantovat projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $projekt = $this->database->table('projekt')->get($id_projektu);

        if ($projekt) {
            $values = array('id_projektu' => $id_projektu, 
                            'id_akademika' => $this->user->id);

            $projekt->update($values);
        }
        else {
            $this->flashMessage('Nelze vložit garanta', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    public function handleRegistrovat($id_projektu)
    {
        if(!$this->getUser()->isInRole('Student')) {
            $this->flashMessage('Nemáte oprávnění registrovat se na projekt', 'alert-box alert');
            $this->redirect('default');
        }

            $tmpSelection = $this->database->table('projekt')->where('typ', 'firemní')->where('stav', 'aktivní')->where('id_akademika NOT', NULL);
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('student_resitel')
                    ->where('projekt_id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->student_id_studenta) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        if (in_array($id_projektu, $seletion)) {
            $values = array('projekt_id_projektu' => $id_projektu, 
                            'student_id_studenta' => $this->user->id);

            $this->database->table('student_resitel')->insert($values);
        }
        else {
            $this->flashMessage('Nelze se registrovat na projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Registrace byla úspěšná', 'alert-box success');
        $this->redirect('default');
    }

    public function handleZastresit($id_projektu)
    {
        if(!$this->getUser()->isInRole('Firma')) {
            $this->flashMessage('Nemáte oprávnění zastřešit projekt', 'alert-box alert');
            $this->redirect('default');
        }

            $tmpSelection = $this->database->table('projekt')->where('typ', 'studentský')->where('stav', 'aktivní')->where('id_akademika NOT', NULL);
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('firma_zastresovatel')
                    ->where('id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->id_firmy) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        if (in_array($id_projektu, $seletion)) {
            $values = array('id_projektu' => $id_projektu, 
                            'id_firmy' => $this->user->id);

            $this->database->table('firma_zastresovatel')->insert($values);
        }
        else {
            $this->flashMessage('Nelze vložit zastřešovatele', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

}
