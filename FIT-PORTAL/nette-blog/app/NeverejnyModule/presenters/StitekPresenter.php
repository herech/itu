<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class StitekPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->stitky = $this->database->table('stitky')->order('nazev');
    }

    public function actionCreate()
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění přidávat štítky', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionEdit($id_stitku)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění upravovat štítek', 'alert-box alert');
            $this->redirect('default');
        }

        $stitek = $this->database->table('stitky')->get($id_stitku);

        if (!$stitek) {
            $this->flashMessage('Šítek nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        $this['upravitStitekForm']->setDefaults($stitek->toArray());
    }

    protected function createComponentUpravitStitekForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('nazev', 'Název štítku')
        ->setRequired('Prosím vyplňte název štítku');
        
        $form->addHidden('id_stitku');

        $form->addSubmit('send', 'Upravit štítek')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'upravitStitekFormSucceeded');

        return $form;
    }

    public function upravitStitekFormSucceeded($form, $values)
    {

        $doslo_k_chybe = false;
        $nektery_stitek_neulozen = array();
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Při ukládání štítků došlo k chybě, pravděpodobně nemáte požadované oprávnění', 'alert-box alert');
            $this->redirect('default'); 
        }
        
            
            if (!$this->database->table('stitky')->where('id_stitku NOT',$values->id_stitku)->where('nazev = ?', $values->nazev)->fetch()) {
                $this->database->table('stitky')->get($values->id_stitku)->update(array("nazev" => $values->nazev));
            }
            else {
                $nektery_stitek_neulozen[] = $values->nazev;
            }         

        if ( $doslo_k_chybe == false) {
            if (empty($nektery_stitek_neulozen)) {
                $this->flashMessage('Štítek upraven', 'alert-box success');
                $this->redirect('default');
            }
            else {
                //$this
                $form->addError('Štítek s daným názvem již existuje');
                $doslo_k_chybe = true;
            }

        }
        else {
            $this->flashMessage('Při ukládání štítků došlo k chybě, pravděpodobně nemáte požadované oprávnění', 'alert-box alert');

            $this->redirect('default'); 
        }
    }

    protected function createComponentPridatStitkyForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('stitky', 'Štítky (seznam štítků oddelených čárkami)')
        ->setAttribute('placeholder', 'Zadejte seznam štítků oddelených ,')
        ->setRequired('Prosím vyplňte seznam štítků');

        $form->addSubmit('send', 'Přidat štítky')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'pridatStitkyFormSucceeded');

        return $form;
    }

    public function pridatStitkyFormSucceeded($form, $values)
    {

        $doslo_k_chybe = false;
        $nektery_stitek_neulozen = array();
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Při ukládání štítků došlo k chybě, pravděpodobně nemáte požadované oprávnění', 'alert-box alert');
            $this->redirect('default'); 
        }
        
            $poleStitku = explode(',', $values->stitky);
            
            // zjistíme jestli se nějaký štítek už v db vyskytuje
            foreach ($poleStitku as $stitek) {
            
                if ($stitek == '') continue;
                if ($this->database->table('stitky')->where('nazev = ?', $stitek)->fetch()) {
                    $nektery_stitek_neulozen[] = $stitek;
                }
            }
            // pokud se nevyskytuje, můžeme věechny štítky uložit
            if (empty($nektery_stitek_neulozen)) {
                foreach ($poleStitku as $stitek) {
                    if ($stitek == '') continue;
                    $this->database->table('stitky')->insert(array('nazev' => $stitek));
                }
                
                $this->flashMessage('Štítky úspěšně uloženy', 'alert-box success');
                $this->redirect('default');
            }
            else {
                $form->addError('Štítky s názvy: ' . implode(", ", $nektery_stitek_neulozen) . ' již existují');
            }


    }

    public function actionDelete($id_stitku)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění mazat štítky', 'alert-box alert');
            $this->redirect('default');
        }

        $stitek = $this->database->table('stitky')->where('id_stitku = ?', $id_stitku)->fetch();

        if (!$stitek) {
            $this->flashMessage('Daný štítek nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $stitek->delete();
            $this->flashMessage('Štítek byl smazán', 'alert-box success');
            $this->redirect('default');
        }
    }
}
