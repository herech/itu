<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class SpravcePresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        $num = $this->database->table('spravce')->count("*");
        if ($num >= 1) {
            if (!$this->user->isLoggedIn()) {
                if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                    $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
                }
                $this->redirect('Sign:in');
            }
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $num = $this->database->table('spravce')->count("*");
        if ($num < 1) {
            $this->redirect('create');
        }
    }

    public function actionCreate()
    {

    }

    public function actionEdit()
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění upravovat správce', 'alert-box alert');
            $this->redirect('default');
        }

        $spravce = $this->database->table('spravce')->get($this->user->id);

        if (!$spravce) {
            $this->flashMessage('Správce nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        $this['spravceForm']->setDefaults($spravce->toArray());

        $this['spravceForm']['login']->setAttribute('placeholder', $spravce->login);
        $this['spravceForm']['login']->setDisabled();
    }

    protected function createComponentSpravceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('login', 'login')
        ->setRequired('Prosím vyplňte svůj login')
        ->setAttribute('placeholder', 'Váš login');

        $form->addText('email', 'email')
        ->setRequired('Prosím vyplňte svůj email')
        ->setAttribute('placeholder', 'Váš email');

        $form->addPassword('heslo', 'Heslo')
        ->setAttribute('placeholder', 'Vaše heslo (alespoň pětimístné)')
        ->addCondition(Form::FILLED)
        ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň 5 znaků', 5);

        $form->addPassword('heslo_overeni', 'Heslo pro kontrolu')
        ->setAttribute('placeholder', 'Zadejte heslo znovu pro ověření')
        ->setOmitted(TRUE)
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['heslo']);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'spravceFormSucceeded');

        return $form;
    }

    public function spravceFormSucceeded($form, $values)
    {
        $num = $this->database->table('spravce')->count("*");
        if ($num >= 1) {
            if(!$this->getUser()->isInRole('Spravce')) {
                $this->flashMessage('Nemáte oprávnění upravovat správce', 'alert-box alert');
                $this->redirect('default');
            }
        }

        $doslo_k_chybe = false;
        $id_spravce = $this->user->id;

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        if ($id_spravce && $this->user->isLoggedIn()) {
            if (isset($values['heslo'])) {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
            }

            $spravce = $this->database->table('spravce')->get($id_spravce);
            $spravce->update($values);
        } else {
            if (empty($values['heslo'])) {
                $form->addError('Zadejte prosím své heslo');
                $doslo_k_chybe = true;
            }
            else {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                $spravce = $this->database->table('spravce')->insert($values);
            }
        }

        if ( $doslo_k_chybe == false) {
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');

            $this->redirect('default');
        }
    }
}
