<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class RozsahPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->rozsahy = $this->database->table('rozsah_projektu');
    }

    public function actionEdit($id_rozsah_projektu)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění upravovat rozsah projektu', 'alert-box alert');
            $this->redirect('default');
        }

        $rozsah = $this->database->table('rozsah_projektu')->get($id_rozsah_projektu);

        if (!$rozsah) {
            $this->flashMessage('Rozsah projektu nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        $this['rozsahForm']->setDefaults($rozsah->toArray());
    }

    protected function createComponentRozsahForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('rozsah', 'Rozsah projektu')
        ->setAttribute('placeholder', 'Rozsah např."malý projekt"')
        ->setAttribute('maxlength', 100)
        ->addRule(Form::MAX_LENGTH, 'Rozsah projektu může mít délků max. %d znaků', 100)
        ->setRequired('Prosím zadejte rozsah projektu');
        
        $form->addTextarea('abstrakt', 'Abstrakt')
        ->setAttribute('placeholder', 'Ůvodní text - bude na hlavní stránce')
        ->setRequired('Musíte vyplnit alespoň abstrakt');
        
        $form->addTextarea('popis', 'Popis')
        ->setAttribute('placeholder', 'Specifikace/pozadavky/informace pro firmy co se zajímají o daný rozsah projektu');

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'rozsahFormSucceeded');

        return $form;
    }

    public function rozsahFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění upravovat rozsah projektu', 'alert-box alert');
            $this->redirect('default');
        }

        $doslo_k_chybe = false;
        $id_rozsah_projektu = $this->getParameter('id_rozsah_projektu');

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        if ($id_rozsah_projektu) {
            $rozsah = $this->database->table('rozsah_projektu')->get($id_rozsah_projektu);
            $rozsah->update($values);
        } else {
            $doslo_k_chybe = true;
        }

        if ( $doslo_k_chybe == false) {
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');
            $this->redirect('default');
        }
    }
}
