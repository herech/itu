<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function actionDefault()
    {
        if ($this->getUser()->isInRole('Akademik')) {
            $this->setView('akademik');
        }
        else if ($this->getUser()->isInRole('Firma')) {
            $this->setView('firma');
        }
        else if ($this->getUser()->isInRole('Student')) {
            $this->setView('student');
        }
    }

    public function renderDefault()
    {
        $this->template->spravce = $this->database->table('spravce')->where('id_spravce', $this->getUser()->getIdentity()->getId())->fetch();
    }

    public function renderAkademik()
    {
        $this->template->akademik = $this->database->table('akademik')->where('id_akademika', $this->getUser()->getIdentity()->getId())->fetch();

        $this->template->garantovane_projekty = $this->database->table('projekt')->where('id_akademika', $this->getUser()->getIdentity()->getId());

        $firmy = $this->database->table('firma')->where('id_akademika', $this->getUser()->getIdentity()->getId());
        $pole = array();
        foreach ($firmy as $key => $value) {
            $pole[] = $value->id_firmy;
        }
        $this->template->odp_firmy = $this->database->table('firma')->where('id_firmy IN', $pole);
    
    }

    public function renderFirma()
    {
        $this->template->firma = $this->database->table('firma')->where('id_firmy', $this->getUser()->getIdentity()->getId())->fetch();

        $zadavatel = $this->database->table('zadavatel_hodnotitel')->where('id_firmy', $this->getUser()->getIdentity()->getId());
        $pole = array();
        foreach ($zadavatel as $key => $value) {
            $pole[] = $value->id_zadavatele_hodnotitele;
        }
        $this->template->vytvorene_projekty = $this->database->table('projekt')->where('id_zadavatele IN', $pole);

        $zastresitel = $this->database->table('firma_zastresovatel')->where('id_firmy', $this->getUser()->getIdentity()->getId());
        $pole = array();
        foreach ($zastresitel as $key => $value) {
            $pole[] = $value->id_projektu;
        }
        $this->template->zastresene_projekty = $this->database->table('projekt')->where('id_projektu IN', $pole);
    
    }

    public function renderStudent()
    {
        $this->template->student = $this->database->table('student')->where('id_studenta', $this->getUser()->getIdentity()->getId())->fetch();
        
        $zadavatel = $this->database->table('zadavatel_hodnotitel')->where('id_studenta', $this->getUser()->getIdentity()->getId());
        $pole = array();
        foreach ($zadavatel as $key => $value) {
            $pole[] = $value->id_zadavatele_hodnotitele;
        }
        $this->template->vytvorene_projekty = $this->database->table('projekt')->where('id_zadavatele IN', $pole);

        $resitel = $this->database->table('student_resitel')->where('student_id_studenta', $this->getUser()->getIdentity()->getId());
        $pole = array();
        foreach ($resitel as $key => $value) {
            $pole[] = $value->projekt_id_projektu;
        }
        $this->template->zapsane_projekty = $this->database->table('projekt')->where('id_projektu IN', $pole);
    }
}
