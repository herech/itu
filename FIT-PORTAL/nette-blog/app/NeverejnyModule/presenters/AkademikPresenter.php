<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class AkademikPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->akademici = $this->database->table('akademik')->order('login');
    }

    public function actionCreate()
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění přidávat akademika', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionGarant()
    {
        if(!$this->getUser()->isInRole('Akademik')) {
            $this->flashMessage('Nemáte oprávnění garantovat projekt', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionUngarant()
    {
        if(!$this->getUser()->isInRole('Akademik')) {
            $this->flashMessage('Nemáte oprávnění zrušit garantaci projektu', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionEdit($id_akademika)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Akademik')) {
            $this->flashMessage('Nemáte oprávnění upravovat akademika', 'alert-box alert');
            $this->redirect('default');
        }

        if($this->getUser()->isInRole('Akademik') && $id_akademika != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění upravovat jiného akademika', 'alert-box alert');
            $this->redirect('default');
        }

        $akademik = $this->database->table('akademik')->get($id_akademika);

        if (!$akademik) {
            $this->flashMessage('Akademik nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        $this['akademikForm']->setDefaults($akademik->toArray());

        $this['akademikForm']['login']->setAttribute('placeholder', $akademik->login);
        $this['akademikForm']['login']->setDisabled();
    }

    protected function createComponentAkademikForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('login', 'login')
        ->setRequired('Prosím vyplňte svůj login')
        ->setAttribute('placeholder', 'Váš login');

        $form->addText('email', 'email')
        ->setRequired('Prosím vyplňte svůj email')
        ->setAttribute('placeholder', 'Váš email');

        $form->addPassword('heslo', 'Heslo')
        ->setAttribute('placeholder', 'Vaše heslo (alespoň pětimístné)')
        ->addCondition(Form::FILLED)
        ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň 5 znaků', 5);

        $form->addPassword('heslo_overeni', 'Heslo pro kontrolu')
        ->setAttribute('placeholder', 'Zadejte heslo znovu pro ověření')
        ->setOmitted(TRUE)
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['heslo']);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'akademikFormSucceeded');

        return $form;
    }

    protected function createComponentGarantForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('id_akademika', $this->user->id);

        $negarantovaneProjekty = $this->database->table('projekt')->where('stav', 'aktivní')
            ->where('id_akademika', NULL);
        $arrProjektu = array();
        foreach ($negarantovaneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Garantovat projekt')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'garantFormSucceeded');

        return $form;
    }

    protected function createComponentUngarantForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('id_akademika', $this->user->id);

        $negarantovaneProjekty = $this->database->table('projekt')->where('stav', 'aktivní')
            ->where('id_akademika', $this->user->id);
        $arrProjektu = array();
        foreach ($negarantovaneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Odgarantovat projekt')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'ungarantFormSucceeded');

        return $form;
    }

    public function akademikFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Akademik')) {
            $this->flashMessage('Nemáte oprávnění upravovat akademika', 'alert-box alert');
            $this->redirect('default');
        }

        $doslo_k_chybe = false;
        $id_akademika = $this->getParameter('id_akademika');

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        if ($id_akademika) {
            if($this->getUser()->isInRole('Akademik') && $id_akademika != $this->user->id) {
                $this->flashMessage('Nemáte oprávnění upravovat jiného akademika', 'alert-box alert');
                $this->redirect('default');
            }

            if (isset($values['heslo'])) {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
            }

            $akademik = $this->database->table('akademik')->get($id_akademika);
            $akademik->update($values);
        } else {
            if(!$this->getUser()->isInRole('Spravce')) {
                $this->flashMessage('Nemáte oprávnění přidávat akademika', 'alert-box alert');
                $this->redirect('default');
            }

            if (empty($values['heslo'])) {
                $form->addError('Zadejte prosím své heslo');
                $doslo_k_chybe = true;
            }
            else {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                $akademik = $this->database->table('akademik')->insert($values);
            }
        }

        if ( $doslo_k_chybe == false) {
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');

            $this->redirect('default');
        }
    }

    public function garantFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Akademik') || $values['id_akademika'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění garantovat projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $projekt = $this->database->table('projekt')->get($values['id_projektu']);

        if ($projekt) {
            $projekt->update($values);
        }
        else {
            $this->flashMessage('Nelze vložit garanta', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Data úspěšně uložena', 'alert-box success');
        $this->redirect('default');
    }

    public function ungarantFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Akademik') || $values['id_akademika'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění garantovat projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $projekt = $this->database->table('projekt')->where('id_projektu', $values['id_projektu'])
            ->where('id_akademika', $values['id_akademika'])->fetch();

        if ($projekt) {
            $values['id_akademika'] = NULL;
            $projekt->update($values);
            $this->flashMessage('Akademik jako garant byl smazán', 'alert-box success');
            $this->redirect('default');
        }
        else {
            $this->flashMessage('Akademik jako garant nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionDelete($id_akademika)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění mazat akademika', 'alert-box alert');
            $this->redirect('default');
        }

        $akademik = $this->database->table('akademik')->get($id_akademika);

        if (!$akademik) {
            $this->flashMessage('Akademik nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $akademik->delete();
            $this->flashMessage('Akademik byl smazán', 'alert-box success');
            $this->redirect('default');
        }
    }
}
