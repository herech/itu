<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;


class SignPresenter extends BasePresenter
{

    /** @persistent */
        //public $backlink = '';

    protected function createComponentSignInForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('login', 'Login')
        ->setRequired('Prosím vyplňte svůj login.')->setAttribute('placeholder', 'Váš login');

        $form->addPassword('password', 'Heslo')
        ->setRequired('Prosím vyplňte své heslo.')->setAttribute('placeholder', 'Vaše heslo');

        $form->addSubmit('send', 'Přihlásit se')->setAttribute('class', 'button');

        $form->onSuccess[] = array($this, 'signInFormSucceeded');
        return $form;
    }

    public function signInFormSucceeded($form)
    {
        $values = $form->values;

        try {
        $this->getUser()->login($values->login, $values->password);
        $this->getUser()->setExpiration('15 minutes', TRUE);
        //$this->restoreRequest($this->backlink);
        $this->redirect('Homepage:default');

        } catch (Nette\Security\AuthenticationException $e) {
        $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('You have been signed out.', 'alert-box info');
        $this->redirect('in');
    }

    public function renderIn()
    {
        $this->template->anyVariable = 'any value';
    }

}
