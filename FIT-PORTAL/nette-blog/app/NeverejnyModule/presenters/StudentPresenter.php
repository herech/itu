<?php

namespace App\NeverejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class StudentPresenter extends BasePresenter
{
    private $database;

    protected function startup()
    {
        parent::startup();

        if (!$this->user->isLoggedIn()) {
            if ($this->user->logoutReason === Nette\Security\IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.', 'alert-box info');
            }
            $this->redirect('Sign:in');
        }
    }

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault()
    {
        $this->template->studenti = $this->database->table('student')->order('login');
    }

    public function actionCreate()
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění přidávat studenta', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionRegister()
    {
        if(!$this->getUser()->isInRole('Student')) {
            $this->flashMessage('Nemáte oprávnění registrovat se na projekt', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionUnregister()
    {
        if(!$this->getUser()->isInRole('Student')) {
            $this->flashMessage('Nemáte oprávnění odregistrovat se z projektu', 'alert-box alert');
            $this->redirect('default');
        }
    }

    public function actionEdit($id_studenta)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Student')) {
            $this->flashMessage('Nemáte oprávnění upravovat studenta', 'alert-box alert');
            $this->redirect('default');
        }

        if($this->getUser()->isInRole('Student') && $id_studenta != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění upravovat jiného studenta', 'alert-box alert');
            $this->redirect('default');
        }

        $student = $this->database->table('student')->get($id_studenta);

        if (!$student) {
            $this->flashMessage('Student nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }

        $this['studentForm']->setDefaults($student->toArray());

        $this['studentForm']['login']->setAttribute('placeholder', $student->login);
        $this['studentForm']['login']->setDisabled();
    }

    protected function createComponentStudentForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('login', 'login')
        ->setRequired('Prosím vyplňte svůj login')
        ->setAttribute('placeholder', 'Váš login');

        $form->addText('email', 'email')
        ->setRequired('Prosím vyplňte svůj email')
        ->setAttribute('placeholder', 'Váš email');

        $form->addPassword('heslo', 'Heslo')
        ->setAttribute('placeholder', 'Vaše heslo (alespoň pětimístné)')
        ->addCondition(Form::FILLED)
        ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň 5 znaků', 5);

        $form->addPassword('heslo_overeni', 'Heslo pro kontrolu')
        ->setAttribute('placeholder', 'Zadejte heslo znovu pro ověření')
        ->setOmitted(TRUE)
        ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['heslo']);

        $form->addSubmit('send', 'Vložit do databáze')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'studentFormSucceeded');

        return $form;
    }

    protected function createComponentRegistraceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('student_id_studenta', $this->user->id);

            $tmpSelection = $this->database->table('projekt')->where('typ', 'firemní')->where('stav', 'aktivní')->where('id_akademika NOT', NULL);
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('student_resitel')
                    ->where('projekt_id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->student_id_studenta) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        $volneProjekty = $this->database->table('projekt')->where('id_projektu', $seletion);
        $arrProjektu = array();
        foreach ($volneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('projekt_id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Registrovat se na projekt')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'registraceFormSucceeded');

        return $form;
    }

    protected function createComponentOdregistraceForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addHidden('student_id_studenta', $this->user->id);

            $zastresene = $this->database->table('student_resitel')->where('student_id_studenta', $this->user->id);
            $seletion = array();
            foreach($zastresene as $tmp) {
                $seletion[] = $tmp->id_projektu;
            }

        $volneProjekty = $this->database->table('projekt')->where('id_projektu', $seletion);
        $arrProjektu = array();
        foreach ($volneProjekty as $projekt) {
            $arrProjektu[$projekt->id_projektu] = $projekt->nazev;
        }
        $form->addSelect('projekt_id_projektu', 'Projekty', $arrProjektu);

        $form->addSubmit('send', 'Odregistrovat se z projektu')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'odregistraceFormSucceeded');

        return $form;
    }

    public function studentFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Spravce') && !$this->getUser()->isInRole('Student')) {
            $this->flashMessage('Nemáte oprávnění upravovat studenta', 'alert-box alert');
            $this->redirect('default');
        }

        $doslo_k_chybe = false;
        $id_studenta = $this->getParameter('id_studenta');

        foreach ($values as $key => $value) if ($value === '') unset($values[$key]);

        if ($id_studenta) {
            if($this->getUser()->isInRole('Student') && $id_studenta != $this->user->id) {
                $this->flashMessage('Nemáte oprávnění upravovat jiného studenta', 'alert-box alert');
                $this->redirect('default');
            }

            if (isset($values['heslo'])) {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
            }

            $student = $this->database->table('student')->get($id_studenta);
            $student->update($values);
        } else {
            if(!$this->getUser()->isInRole('Spravce')) {
                $this->flashMessage('Nemáte oprávnění přidávat studenta', 'alert-box alert');
                $this->redirect('default');
            }

            if (empty($values['heslo'])) {
                $form->addError('Zadejte prosím své heslo');
                $doslo_k_chybe = true;
            }
            else {
                $values['heslo'] = Nette\Security\Passwords::hash($values['heslo']);
                $student = $this->database->table('student')->insert($values);
            }
        }

        if ( $doslo_k_chybe == false) {
            $this->flashMessage('Data úspěšně uložena', 'alert-box success');

            $this->redirect('default');
        }
    }

    public function registraceFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Student') || $values['student_id_studenta'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění registrovat se na projekt', 'alert-box alert');
            $this->redirect('default');
        }

            $tmpSelection = $this->database->table('projekt')->where('typ', 'firemní')->where('stav', 'aktivní')->where('id_akademika NOT', NULL);
            $seletion = array();
            foreach($tmpSelection as $tmpProjekt) {
                $prihlaseni = $this->database->table('student_resitel')
                    ->where('projekt_id_projektu', $tmpProjekt->id_projektu);
                $muzu = true;
                foreach ($prihlaseni as $row) {
                    if ($this->user->id == $row->student_id_studenta) {
                        $muzu = false;
                    }
                }
                if ($prihlaseni->count("*") < $tmpProjekt->max_pocet_prihlasenych && $muzu) {
                    $seletion[] = $tmpProjekt->id_projektu;
                }
            }

        if (in_array($values->projekt_id_projektu, $seletion)) {
            $this->database->table('student_resitel')->insert($values);
        }
        else {
            $this->flashMessage('Nelze se registrovat na projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $this->flashMessage('Registrace byla úspěšná', 'alert-box success');
        $this->redirect('default');
    }

    public function odregistraceFormSucceeded($form, $values)
    {
        if(!$this->getUser()->isInRole('Student') || $values['student_id_studenta'] != $this->user->id) {
            $this->flashMessage('Nemáte oprávnění registrovat se na projekt', 'alert-box alert');
            $this->redirect('default');
        }

        $student_resitel = $this->database->table('student_resitel')
            ->where('student_id_studenta', $values['student_id_studenta'])
            ->where('projekt_id_projektu', $values['projekt_id_projektu'])->fetch();

        if (!$student_resitel) {
            $this->flashMessage('Nebyl jse odregistrován', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $student_resitel->delete();
            $this->flashMessage('Byl jste odregistrován', 'alert-box success');
            $this->redirect('default');
        }
    }

    public function actionDelete($id_studenta)
    {
        if(!$this->getUser()->isInRole('Spravce')) {
            $this->flashMessage('Nemáte oprávnění mazat studenta', 'alert-box alert');
            $this->redirect('default');
        }

        $student = $this->database->table('student')->get($id_studenta);

        if (!$student) {
            $this->flashMessage('Student nebyl nalezen', 'alert-box alert');
            $this->redirect('default');
        }
        else {
            $student->delete();
            $this->flashMessage('Student byl smazán', 'alert-box success');
            $this->redirect('default');
        }
    }
}
