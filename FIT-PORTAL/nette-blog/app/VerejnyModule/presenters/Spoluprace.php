<?php

namespace App\VerejnyModule\Presenters;

use Nette;
use App\Model;


class SpolupracePresenter extends BasePresenter
{
	public function renderDefault()
	{
		$this->redirect(':Verejny:Spoluprace:Drobne');
	}

    public function renderDrobne()
    {
        $this->template->anyVariable = 'any value';
    }

    public function renderStredni()
    {
        $this->template->anyVariable = 'any value';
    }

    public function renderVetsi()
    {
        $this->template->anyVariable = 'any value';
    }

    public function renderCzEu()
    {
        $this->template->anyVariable = 'any value';
    }

    public function renderSponzoring()
    {
        $this->template->anyVariable = 'any value';
    }
}
