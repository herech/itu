<?php

namespace App\VerejnyModule\Presenters;

use Nette;
use App\Model;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Application\UI\Form;
use Tracy\Debugger;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    protected function createComponentKontaktForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('firma', 'Firma')
        ->setRequired('Prosím vyplňte název firmy, kterou zastupujete')
        ->setAttribute('placeholder', 'Název firmy, kterou zastupujete');

        $form->addText('email', 'Email')
        ->setType('email')
        ->setRequired('Prosím vyplňte váš kontaktní email')
        ->setAttribute('placeholder', 'Váš kontaktní email')
        ->addRule(Form::EMAIL, 'Prosím zadejte platnou emailovou adresu');

        $typy_projektu = array('drobne'     => 'Drobné projekty',
                               'stredni'    => 'Střední projekty',
                               'vetsi'      => 'Větší projekty',
                               'czeu'       => 'Tuzemské a evropské projekty',
                               'sponzoring' => 'Sponzoring akcí');
        $form->addCheckboxList('projekty', 'Jako firma máme zájem o následující typy projektů:', $typy_projektu);

        $form->addTextArea('komentar', 'Komentář')
        ->setAttribute('placeholder', 'Sdělte nám Váš případný komentář či otázku')
        ->addRule(Form::MAX_LENGTH, 'Komentář je příliš dlouhý', 1000);

        $form->addSubmit('send', 'Odeslat')->setAttribute('class', 'button');
        $form->onSuccess[] = array($this, 'kontaktFormSucceeded');

        return $form;
    }

    public function kontaktFormSucceeded($form, array $values)
    {
        $mailer = new SendmailMailer;                // odkomentovat az to bude na serva
        /*$values['email'] = 'fit-portal@seznam.cz'; // smazat az to bude na serva
        $mailer = new Nette\Mail\SmtpMailer(array(
            'host' => 'smtp.seznam.cz',
            'username' => 'fit-portal@seznam.cz',
            'password' => 'Portal0',
            'secure' => 'ssl',
        ));*/
        $mail = new Message;

        $chyba = false;
        if (empty($values['projekty']) && $values['komentar'] === '') {
            $chyba = true;
        }

        if (!$chyba) {
            $typy_projektu = array('drobne'     => 'Drobné projekty',
                                   'stredni'    => 'Střední projekty',
                                   'vetsi'      => 'Větší projekty',
                                   'czeu'       => 'Tuzemské a evropské projekty',
                                   'sponzoring' => 'Sponzoring akcí');

            $body  = "<h3>Firma: " . $values['firma'] . "</h3>";
            $body .= "<h4>email: " . $values['email'] . "</h4>";

            if (!empty($values['projekty'])) {
                $body .= "<h4>Má zájem o:</h4><ul>";

                foreach ($values['projekty'] as $key => $val)
                    $body .= "<li>" . $typy_projektu[$val] . "</li>";

                $body .= "</ul>";
            } else {
                $body .= "<h4>Odeslala pouze komentář</h4>";
            }

            if ($values['komentar'] !== '') {
                $body .= "<h4>Komentář:</h4><ul>" . $values['komentar'] . "</ul>";
            }

            $admin_email = $this->database->table('spravce')->fetch()->email;
            $mail->setFrom($values['email'])
                 ->addTo($admin_email)
                 ->setSubject($values['firma'])
                 ->setHTMLBody($body);

            $mailer->send($mail);

            $this->flashMessage('Zpráva úspěšně odeslána', 'nette-alert-box-success');
            $this->redirect('this');
        } else {
            $this->flashMessage('Prosím zvolte Váš zájem, nebo zadejte komentář', 'nette-alert-box-alert');
        }
    }
}
