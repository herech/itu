<?php

namespace App\VerejnyModule\Presenters;

use Nette;
use App\Model;


class VysledkyPresenter extends BasePresenter
{
    
    private $database;

	public function renderDefault()
	{
        $this->template->database = $this->database;
		$this->template->projekty = $this->database->table('projekt')->where('stav = ? AND verejny = ?', "dokončený", 1)->order('nazev');
	}
    
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }
}
