-- phpMyAdmin SQL Dump
-- version 4.4.6.1
-- http://www.phpmyadmin.net
--
-- Počítač: localhost:3306
-- Vytvořeno: Ned 13. pro 2015, 23:31
-- Verze serveru: 5.6.27-log
-- Verze PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `czfit-portal`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `akademik`
--

CREATE TABLE IF NOT EXISTS `akademik` (
  `id_akademika` int(11) NOT NULL,
  `login` varchar(10) NOT NULL,
  `heslo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `akademik`
--

INSERT INTO `akademik` (`id_akademika`, `login`, `heslo`, `email`) VALUES
(4, 'hruby', '$2y$10$rQBCX/wd6n/DOVmCFJjVt.zFZddqRueVCZafG5ucnnltKLBYnJ.n6', 'hrubym@fit.vutbr.cz'),
(5, 'akademik', '$2y$10$iG5L038YjLBgOoMPu.5mi.Jig3EuEnskyVh.VOcUkD8jbmqZa6FYS', 'akademik@fit.vutbr.cz'),
(6, 'rysavy', '$2y$10$tTHqgiqrQdopdumitodjhe5mvuMuHkWcPrrWsbf8flpg.S4X6FOkW', 'rysavy@fit.vutbr.cz'),
(7, 'beran', '$2y$10$FrqTRvpE3bGakhMfLBibEO/wLDsFm/Xl/bShA.7tt.nBAdLODtVFW', 'beranv@fit.vutbr.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `firma`
--

CREATE TABLE IF NOT EXISTS `firma` (
  `id_firmy` int(11) NOT NULL,
  `login` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `heslo` varchar(100) NOT NULL,
  `id_akademika` int(11) NOT NULL,
  `nazev` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `firma`
--

INSERT INTO `firma` (`id_firmy`, `login`, `email`, `heslo`, `id_akademika`, `nazev`) VALUES
(3, 'mozilla', 'mozilla@mozilla.cz', '$2y$10$ux7c2SZZe9MMIqSGNmEijuFa.YMTpe6BXN23d8JUqdi1j.tiPLpFK', 7, ''),
(4, 'ubuntu', 'ubuntu@ubuntu.cz', '$2y$10$t/.Nzxp92pkJOy4LE3NGcuyyKS77uSt/11OMjBJOJb6DWn6nESR.m', 6, ''),
(6, 'intel', 'intel@intel.cz', '$2y$10$NGjnIKFvnuKFAfrg/Oxc1.00x0tTcVAFk75rgeyB9RZz/hRvaYJZm', 6, ''),
(7, 'firma', 'firma@firma.cz', '$2y$10$9N2QhT07L.pYy/TClYqQJ.e2WRvGA1I41UNNscjWXJ3ovENIAXc/u', 5, '');

-- --------------------------------------------------------

--
-- Struktura tabulky `firma_rozsah`
--

CREATE TABLE IF NOT EXISTS `firma_rozsah` (
  `id_firmy` int(11) NOT NULL,
  `id_rozsahu_projektu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `firma_rozsah`
--

INSERT INTO `firma_rozsah` (`id_firmy`, `id_rozsahu_projektu`) VALUES
(3, 1),
(3, 2),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `firma_zastresovatel`
--

CREATE TABLE IF NOT EXISTS `firma_zastresovatel` (
  `id_firmy` int(11) NOT NULL,
  `id_projektu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `firma_zastresovatel`
--

INSERT INTO `firma_zastresovatel` (`id_firmy`, `id_projektu`) VALUES
(3, 34),
(3, 35);

-- --------------------------------------------------------

--
-- Struktura tabulky `hodnoceni`
--

CREATE TABLE IF NOT EXISTS `hodnoceni` (
  `id_projektu` int(11) NOT NULL,
  `id_zadavatele_hodnotitele` int(11) NOT NULL,
  `komentar` tinytext,
  `znamka` enum('1','2','3','4','5') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `hodnoceni`
--

INSERT INTO `hodnoceni` (`id_projektu`, `id_zadavatele_hodnotitele`, `komentar`, `znamka`) VALUES
(32, 55, 'Zajímavý projekt, mnoho jsem se naučil', '4'),
(32, 56, 'Moc mě to nebavilo :-P', '1');

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt`
--

CREATE TABLE IF NOT EXISTS `projekt` (
  `id_projektu` int(11) NOT NULL,
  `nazev` varchar(100) NOT NULL,
  `typ` enum('firemní','studentský','','') NOT NULL,
  `popis_zadani` mediumtext NOT NULL,
  `popis_pozadovanych_vysledku` varchar(200) NOT NULL DEFAULT 'Bude upřesněno',
  `id_akademika` int(11) DEFAULT NULL,
  `popis_dosazenych_vysledku` mediumtext,
  `id_zadavatele` int(11) NOT NULL,
  `id_rozsahu_projektu` int(11) NOT NULL,
  `stav` enum('aktivní','dokončený','zrušený') NOT NULL DEFAULT 'aktivní',
  `verejny` tinyint(1) NOT NULL DEFAULT '0',
  `max_pocet_prihlasenych` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `projekt`
--

INSERT INTO `projekt` (`id_projektu`, `nazev`, `typ`, `popis_zadani`, `popis_pozadovanych_vysledku`, `id_akademika`, `popis_dosazenych_vysledku`, `id_zadavatele`, `id_rozsahu_projektu`, `stav`, `verejny`, `max_pocet_prihlasenych`) VALUES
(30, 'Implementační, ladící a testovací práce', 'firemní', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Předmětem smluvn&iacute;ho v&yacute;zkumu jsou implementačn&iacute;, testovac&iacute; a lad&iacute;c&iacute; pr&aacute;ce na softwarech Nemea, IPFIXcol, Netppeer a na firmwarech pro karty COMBO-100G.</span></p>', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Předmětem smluvn&iacute;ho v&yacute;zkumu jsou implementačn&iacute;, testovac&iacute; a la', 4, '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Předmětem smluvn&iacute;ho v&yacute;zkumu jsou implementačn&iacute;, testovac&iacute; a lad&iacute;c&iacute; pr&aacute;ce na softwarech Nemea, IPFIXcol, Netppeer a na firmwarech pro karty COMBO-100G.</span></p>', 49, 1, 'aktivní', 1, 3),
(31, 'Multifunkční obvodové prvky na bázi hybridní organické elektroniky', 'firemní', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Projekt se zab&yacute;v&aacute; t&eacute;matikou hybridn&iacute;ch organick&yacute;ch tranzistorů ř&iacute;zen&yacute;ch polem (OFET), charakterizac&iacute; těchto tranzistorů a identifikac&iacute; parametrů důležit&yacute;ch z pohledu logick&yacute;ch obvodů. Na z&aacute;kladě zji&scaron;těn&yacute;ch skutečnost&iacute; bude přistoupeno k n&aacute;vrhu a fyzick&eacute; realizaci jednoduch&eacute;ho logick&eacute;ho obvodu. Dosažen&eacute; v&yacute;sledky najdou uplatněn&iacute; při konstrukci složitěj&scaron;&iacute;ch obvodov&yacute;ch celků s př&iacute;padnou perspektivou průmyslov&eacute; aplikace. Tento syst&eacute;m technologick&yacute;ch postupů a realizace tranzistorů bude unik&aacute;tn&iacute; ve středn&iacute; Evropě a budou do něj zapojeni studenti doktorsk&eacute;ho studia, kteř&iacute; se mohou st&aacute;t průkopn&iacute;ky t&eacute;to technologie v průmyslu.</span></p>', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Projekt se zab&yacute;v&aacute; t&eacute;matikou hybridn&iacute;ch organick&yacute;ch tran', 4, '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Projekt se zab&yacute;v&aacute; t&eacute;matikou hybridn&iacute;ch organick&yacute;ch tranzistorů ř&iacute;zen&yacute;ch polem (OFET), charakterizac&iacute; těchto tranzistorů a identifikac&iacute; parametrů důležit&yacute;ch z pohledu logick&yacute;ch obvodů. Na z&aacute;kladě zji&scaron;těn&yacute;ch skutečnost&iacute; bude přistoupeno k n&aacute;vrhu a fyzick&eacute; realizaci jednoduch&eacute;ho logick&eacute;ho obvodu. Dosažen&eacute; v&yacute;sledky najdou uplatněn&iacute; při konstrukci složitěj&scaron;&iacute;ch obvodov&yacute;ch celků s př&iacute;padnou perspektivou průmyslov&eacute; aplikace. Tento syst&eacute;m technologick&yacute;ch postupů a realizace tranzistorů bude unik&aacute;tn&iacute; ve středn&iacute; Evropě a budou do něj zapojeni studenti doktorsk&eacute;ho studia, kteř&iacute; se mohou st&aacute;t průkopn&iacute;ky t&eacute;to technologie v průmyslu.</span></p>', 50, 2, 'aktivní', 1, 4),
(32, 'Application for hematoma recognition in iOS environment', 'firemní', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem projektu je n&aacute;vrh a v&yacute;voj aplikace pro prostřed&iacute; iOS, kter&aacute; bude rozpozn&aacute;vat hematomy (tvar a obsah) na lidsk&eacute; kůži.</span></p>', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem projektu je n&aacute;vrh a v&yacute;voj aplikace pro prostřed&iacute; iOS, kt', 7, '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem projektu je n&aacute;vrh a v&yacute;voj aplikace pro prostřed&iacute; iOS, kter&aacute; bude rozpozn&aacute;vat hematomy (tvar a obsah) na lidsk&eacute; kůži.</span></p>', 51, 3, 'dokončený', 1, 7),
(33, 'Dodání akustického modelu, jazykového modelu a výslovnostního slovníku pro španělský jazyk', 'firemní', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem je dod&aacute;n&iacute;&nbsp;akustick&eacute;ho modelu, jazykov&eacute;ho modelu a v&yacute;slovnostn&iacute;ho slovn&iacute;ku pro &scaron;panělsk&yacute; jazyk, včetně v&scaron;ech požadavků ohledně slov, fon&eacute;mů a dal&scaron;&iacute;ch parametrů.</span></p>', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem je dod&aacute;n&iacute;&nbsp;akustick&eacute;ho modelu, jazykov&eacute;ho mod', 7, '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">C&iacute;lem je dod&aacute;n&iacute;&nbsp;akustick&eacute;ho modelu, jazykov&eacute;ho modelu a v&yacute;slovnostn&iacute;ho slovn&iacute;ku pro &scaron;panělsk&yacute; jazyk, včetně v&scaron;ech požadavků ohledně slov, fon&eacute;mů a dal&scaron;&iacute;ch parametrů.</span></p>', 52, 2, 'dokončený', 1, 32),
(34, 'Pokročilé metody pro klasifikaci mentálních stavů analýzou elektrické mozkové aktivity', 'studentský', '<table style="border-collapse: collapse; margin-top: 0px; margin-bottom: 0px; border-spacing: 0px; font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;" border="0" width="100%" cellspacing="0" cellpadding="2">\n<tbody>\n<tr>\n<td colspan="2">C&iacute;lem projektu je vytvořen&iacute; komplexn&iacute; metodiky, hardwarov&eacute;ho vybaven&iacute; a software, kter&yacute; umožn&iacute; sn&iacute;m&aacute;n&iacute;, archivaci a anal&yacute;zu elektroencefalografick&yacute;ch dat za &uacute;čelem rozpozn&aacute;n&iacute; ment&aacute;ln&iacute;ho či emočn&iacute;ho stavu subjektu.</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>', '<table style="border-collapse: collapse; margin-top: 0px; margin-bottom: 0px; border-spacing: 0px; font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;" border=', 7, '<table style="border-collapse: collapse; margin-top: 0px; margin-bottom: 0px; border-spacing: 0px; font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;" border="0" width="100%" cellspacing="0" cellpadding="2">\n<tbody>\n<tr>\n<td colspan="2">C&iacute;lem projektu je vytvořen&iacute; komplexn&iacute; metodiky, hardwarov&eacute;ho vybaven&iacute; a software, kter&yacute; umožn&iacute; sn&iacute;m&aacute;n&iacute;, archivaci a anal&yacute;zu elektroencefalografick&yacute;ch dat za &uacute;čelem rozpozn&aacute;n&iacute; ment&aacute;ln&iacute;ho či emočn&iacute;ho stavu subjektu.</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>', 53, 3, 'aktivní', 1, 3),
(35, 'Advanced Methods for Evolutionary Design of Complex Digital Circuits', 'studentský', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Evolučn&iacute; n&aacute;vrh obvodů je metoda, kter&aacute; použ&iacute;v&aacute; biologi&iacute; inspirovan&eacute; prohled&aacute;vac&iacute; algoritmy pro synt&eacute;zu a optimalizaci elektronick&yacute;ch obvodů. Ačkoliv evolučn&iacute; n&aacute;vrh umožnil z&iacute;skat mnoho zaj&iacute;mav&yacute;ch v&yacute;sledků, nemožnost dobr&eacute; &scaron;k&aacute;lovatelnosti zůst&aacute;v&aacute; hlavn&iacute; nev&yacute;hodou metody. V&nbsp;tomto projektu navrhujeme a obhajujeme nov&eacute; př&iacute;stupy k&nbsp;evolučn&iacute;mu n&aacute;vrhu obvodů, kter&eacute; umožn&iacute; eliminovat probl&eacute;m &scaron;k&aacute;lovatelnosti a dovol&iacute; vytv&aacute;řet a optimalizovat složit&eacute; č&iacute;slicov&eacute; obvody a adaptivn&iacute; hardware v&nbsp;programovateln&yacute;ch hradlov&yacute;ch pol&iacute;ch (FPGA). Navrhovan&yacute; projekt předpokl&aacute;d&aacute; n&aacute;sleduj&iacute;c&iacute; v&yacute;sledky: (1) Nov&eacute; algoritmy pro ověřov&aacute;n&iacute; funkčn&iacute; ekvivalence, kter&eacute; umožn&iacute; urychlit v&yacute;počet fitness. (2) Nov&eacute; v&iacute;cekriteri&aacute;ln&iacute; a koevolučn&iacute; algoritmy pro evolučn&iacute; n&aacute;vrh. (3) Rutinn&iacute; n&aacute;vrh a optimalizace složit&yacute;ch č&iacute;slicov&yacute;ch obvodů pomoc&iacute; navržen&yacute;ch metod. (4) Nov&eacute; architektury pro vyv&iacute;jej&iacute;c&iacute; se a adaptivn&iacute; syst&eacute;my implementovan&eacute; v FPGA.</span></p>', '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Evolučn&iacute; n&aacute;vrh obvodů je metoda, kter&aacute; použ&iacute;v&aacute; biologi&', 7, '<p><span style="font-family: ''Open Sans'', Verdana, Tahoma, sans-serif; font-size: 13px; line-height: 16.9px;">Evolučn&iacute; n&aacute;vrh obvodů je metoda, kter&aacute; použ&iacute;v&aacute; biologi&iacute; inspirovan&eacute; prohled&aacute;vac&iacute; algoritmy pro synt&eacute;zu a optimalizaci elektronick&yacute;ch obvodů. Ačkoliv evolučn&iacute; n&aacute;vrh umožnil z&iacute;skat mnoho zaj&iacute;mav&yacute;ch v&yacute;sledků, nemožnost dobr&eacute; &scaron;k&aacute;lovatelnosti zůst&aacute;v&aacute; hlavn&iacute; nev&yacute;hodou metody. V&nbsp;tomto projektu navrhujeme a obhajujeme nov&eacute; př&iacute;stupy k&nbsp;evolučn&iacute;mu n&aacute;vrhu obvodů, kter&eacute; umožn&iacute; eliminovat probl&eacute;m &scaron;k&aacute;lovatelnosti a dovol&iacute; vytv&aacute;řet a optimalizovat složit&eacute; č&iacute;slicov&eacute; obvody a adaptivn&iacute; hardware v&nbsp;programovateln&yacute;ch hradlov&yacute;ch pol&iacute;ch (FPGA). Navrhovan&yacute; projekt předpokl&aacute;d&aacute; n&aacute;sleduj&iacute;c&iacute; v&yacute;sledky: (1) Nov&eacute; algoritmy pro ověřov&aacute;n&iacute; funkčn&iacute; ekvivalence, kter&eacute; umožn&iacute; urychlit v&yacute;počet fitness. (2) Nov&eacute; v&iacute;cekriteri&aacute;ln&iacute; a koevolučn&iacute; algoritmy pro evolučn&iacute; n&aacute;vrh. (3) Rutinn&iacute; n&aacute;vrh a optimalizace složit&yacute;ch č&iacute;slicov&yacute;ch obvodů pomoc&iacute; navržen&yacute;ch metod. (4) Nov&eacute; architektury pro vyv&iacute;jej&iacute;c&iacute; se a adaptivn&iacute; syst&eacute;my implementovan&eacute; v FPGA.</span></p>', 54, 2, 'dokončený', 1, 4);

-- --------------------------------------------------------

--
-- Struktura tabulky `projekt_stitky`
--

CREATE TABLE IF NOT EXISTS `projekt_stitky` (
  `id_stitku` int(11) NOT NULL,
  `id_projektu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `projekt_stitky`
--

INSERT INTO `projekt_stitky` (`id_stitku`, `id_projektu`) VALUES
(14, 30),
(15, 30),
(16, 30),
(17, 30),
(18, 31),
(19, 31),
(20, 31),
(21, 32),
(22, 32),
(23, 32),
(24, 32),
(25, 33),
(26, 33),
(27, 33),
(28, 33),
(29, 33),
(30, 33),
(31, 34),
(32, 34),
(33, 35),
(34, 35),
(35, 35),
(36, 35);

-- --------------------------------------------------------

--
-- Struktura tabulky `rozsah_projektu`
--

CREATE TABLE IF NOT EXISTS `rozsah_projektu` (
  `id_rozsah_projektu` int(11) NOT NULL,
  `rozsah` varchar(100) NOT NULL,
  `popis` mediumtext,
  `abstrakt` tinytext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `rozsah_projektu`
--

INSERT INTO `rozsah_projektu` (`id_rozsah_projektu`, `rozsah`, `popis`, `abstrakt`) VALUES
(1, 'drobný projekt', 'Projekty vhodné do předmětů s rozsahem 1-3 měsíce, nízká finanční náročnost, vhodné pro drobné testování a menší problémy, IP firmy. ', 'Projekty vhodné do předmětů s rozsahem 1-3 měsíce, nízká finanční náročnost, vhodné pro drobné testování a menší problémy, IP firmy. '),
(2, 'střední projekt', 'Projekty vhodné jako diplomová práce, delší doba řešení (6-8 měsíců), nízká finanční náročnost, vhodné pro zkoumání rizikových nápadů nebo vývoje základních technologií, IP dle dohody. ', 'Projekty vhodné jako diplomová práce, delší doba řešení (6-8 měsíců), nízká finanční náročnost, vhodné pro zkoumání rizikových nápadů nebo vývoje základních technologií, IP dle dohody. '),
(3, 'větší projekt', 'Projekty vhodné pro vývoj složitějších řešení, vyšší finanční náročnost, časová zátěž dle domluvy, forma smluvního výzkumu, IP dle domluvy.', 'Projekty vhodné pro vývoj složitějších řešení, vyšší finanční náročnost, časová zátěž dle domluvy, forma smluvního výzkumu, IP dle domluvy.');

-- --------------------------------------------------------

--
-- Struktura tabulky `spravce`
--

CREATE TABLE IF NOT EXISTS `spravce` (
  `id_spravce` int(11) NOT NULL,
  `login` varchar(10) NOT NULL,
  `heslo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `spravce`
--

INSERT INTO `spravce` (`id_spravce`, `login`, `heslo`, `email`) VALUES
(1, 'admin', '$2y$10$mOq3/FmOEwzfdTFTKKzDYOOxZdH7EEoTG6.GkAh85Q6ZFiY0h0T.u', 'fit-portal@seznam.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `stitky`
--

CREATE TABLE IF NOT EXISTS `stitky` (
  `id_stitku` int(11) NOT NULL,
  `nazev` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `stitky`
--

INSERT INTO `stitky` (`id_stitku`, `nazev`) VALUES
(11, 'C++'),
(12, 'Java'),
(13, 'C#'),
(14, 'implementace'),
(15, 'testování'),
(16, 'software'),
(17, 'firmware'),
(18, 'hybridní OFET'),
(19, 'polymorfní elektronika'),
(20, 'multifunkční číslicové obvody'),
(21, 'hematom'),
(22, 'iOS'),
(23, 'zpracování obrazu'),
(24, 'lidská kůže'),
(25, 'akustický model'),
(26, 'jazykový model'),
(27, 'výslovnostní slovník'),
(28, 'slova'),
(29, 'slovník'),
(30, 'španělský jazyk'),
(31, 'elektroencefalografie'),
(32, 'mentální stavy'),
(33, 'Evoluční návrh obvodů'),
(34, 'evoluční hardware'),
(35, 'programovatelná hradlová pole'),
(36, 'koevoluce');

-- --------------------------------------------------------

--
-- Struktura tabulky `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id_studenta` int(11) NOT NULL,
  `login` varchar(10) NOT NULL,
  `heslo` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `student`
--

INSERT INTO `student` (`id_studenta`, `login`, `heslo`, `email`) VALUES
(4, 'xjuhan01', '$2y$10$wEckg52IFqCct9e3jr.We.vY38mhH2yT4RSJ90kY1nXqEC038IUlC', 'xjuhan01@stud.fit.vutbr.cz'),
(5, 'xherec00', '$2y$10$ojJ7FWBPk06DZAO9Gb.TkO2kJ1X3PguraCLPMNGGJSZAYx/d13Z6u', 'xherec00@stud.fit.vutbr.cz'),
(6, 'student', '$2y$10$yvV.GjWCZUV3alFiQRWmT.s.EdDAVKnRw4Yv0hg7Eb0Gbipli0uzm', 'student@stud.fit.vutbr.cz'),
(7, 'xbankt00', '$2y$10$tqZi73ZWN.IcxY5WqGm4G.xlNwuukyErIefGDWIQcpwDOzy/Z6mxi', 'xbankt00@stud.fit.vutbr.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `student_resitel`
--

CREATE TABLE IF NOT EXISTS `student_resitel` (
  `projekt_id_projektu` int(11) NOT NULL,
  `student_id_studenta` int(11) NOT NULL,
  `cesta_k_technicke_zprave` varchar(100) DEFAULT NULL,
  `cesta_k_vystupnimu_archivu` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `student_resitel`
--

INSERT INTO `student_resitel` (`projekt_id_projektu`, `student_id_studenta`, `cesta_k_technicke_zprave`, `cesta_k_vystupnimu_archivu`) VALUES
(30, 4, NULL, NULL),
(30, 5, NULL, NULL),
(31, 4, NULL, NULL),
(31, 5, NULL, NULL),
(32, 4, NULL, NULL),
(32, 5, NULL, NULL),
(33, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `zadavatel_hodnotitel`
--

CREATE TABLE IF NOT EXISTS `zadavatel_hodnotitel` (
  `id_zadavatele_hodnotitele` int(11) NOT NULL,
  `typ` enum('akademik','student','firma') NOT NULL DEFAULT 'student',
  `id_firmy` int(11) DEFAULT NULL,
  `id_akademika` int(11) DEFAULT NULL,
  `id_studenta` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `zadavatel_hodnotitel`
--

INSERT INTO `zadavatel_hodnotitel` (`id_zadavatele_hodnotitele`, `typ`, `id_firmy`, `id_akademika`, `id_studenta`) VALUES
(40, 'student', NULL, NULL, NULL),
(41, 'student', NULL, NULL, NULL),
(42, 'firma', NULL, NULL, NULL),
(43, 'akademik', NULL, NULL, NULL),
(44, 'student', NULL, NULL, NULL),
(45, 'student', NULL, NULL, NULL),
(46, 'student', NULL, NULL, NULL),
(47, 'firma', NULL, NULL, NULL),
(48, 'student', NULL, NULL, NULL),
(49, 'firma', 4, NULL, NULL),
(50, 'firma', 6, NULL, NULL),
(51, 'firma', 3, NULL, NULL),
(52, 'firma', 3, NULL, NULL),
(53, 'student', NULL, NULL, 5),
(54, 'student', NULL, NULL, 4),
(55, 'student', NULL, NULL, 5),
(56, 'student', NULL, NULL, 4);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `akademik`
--
ALTER TABLE `akademik`
  ADD PRIMARY KEY (`id_akademika`);

--
-- Klíče pro tabulku `firma`
--
ALTER TABLE `firma`
  ADD PRIMARY KEY (`id_firmy`),
  ADD KEY `fk_firma_akademik_idx` (`id_akademika`);

--
-- Klíče pro tabulku `firma_rozsah`
--
ALTER TABLE `firma_rozsah`
  ADD PRIMARY KEY (`id_firmy`,`id_rozsahu_projektu`),
  ADD KEY `fk_table1_firma1_idx` (`id_firmy`),
  ADD KEY `fk_table1_rozsah_projektu1_idx` (`id_rozsahu_projektu`);

--
-- Klíče pro tabulku `firma_zastresovatel`
--
ALTER TABLE `firma_zastresovatel`
  ADD PRIMARY KEY (`id_firmy`,`id_projektu`),
  ADD KEY `fk_firma_zastresovatel_firma1_idx` (`id_firmy`),
  ADD KEY `fk_firma_zastresovatel_projekt1_idx` (`id_projektu`);

--
-- Klíče pro tabulku `hodnoceni`
--
ALTER TABLE `hodnoceni`
  ADD PRIMARY KEY (`id_projektu`,`id_zadavatele_hodnotitele`),
  ADD KEY `fk_hodnoceni_projekt1_idx` (`id_projektu`),
  ADD KEY `fk_hodnoceni_zadavatel_hodnotitel1_idx` (`id_zadavatele_hodnotitele`);

--
-- Klíče pro tabulku `projekt`
--
ALTER TABLE `projekt`
  ADD PRIMARY KEY (`id_projektu`),
  ADD KEY `fk_projekt_akademik1_idx` (`id_akademika`),
  ADD KEY `fk_projekt_zadavatel_hodnotitel1_idx` (`id_zadavatele`),
  ADD KEY `fk_projekt_rozsah_projektu1_idx` (`id_rozsahu_projektu`);

--
-- Klíče pro tabulku `projekt_stitky`
--
ALTER TABLE `projekt_stitky`
  ADD PRIMARY KEY (`id_stitku`,`id_projektu`),
  ADD KEY `fk_projekt_stitky_stitky1_idx` (`id_stitku`),
  ADD KEY `fk_projekt_stitky_projekt1_idx` (`id_projektu`);

--
-- Klíče pro tabulku `rozsah_projektu`
--
ALTER TABLE `rozsah_projektu`
  ADD PRIMARY KEY (`id_rozsah_projektu`);

--
-- Klíče pro tabulku `spravce`
--
ALTER TABLE `spravce`
  ADD PRIMARY KEY (`id_spravce`);

--
-- Klíče pro tabulku `stitky`
--
ALTER TABLE `stitky`
  ADD PRIMARY KEY (`id_stitku`);

--
-- Klíče pro tabulku `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_studenta`);

--
-- Klíče pro tabulku `student_resitel`
--
ALTER TABLE `student_resitel`
  ADD PRIMARY KEY (`projekt_id_projektu`,`student_id_studenta`),
  ADD KEY `fk_student_resitel_projekt1_idx` (`projekt_id_projektu`),
  ADD KEY `fk_student_resitel_student1_idx` (`student_id_studenta`);

--
-- Klíče pro tabulku `zadavatel_hodnotitel`
--
ALTER TABLE `zadavatel_hodnotitel`
  ADD PRIMARY KEY (`id_zadavatele_hodnotitele`),
  ADD KEY `fk_zadavatel_hodnotitel_firma1_idx` (`id_firmy`),
  ADD KEY `fk_zadavatel_hodnotitel_akademik1_idx` (`id_akademika`),
  ADD KEY `fk_zadavatel_hodnotitel_student1_idx` (`id_studenta`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `akademik`
--
ALTER TABLE `akademik`
  MODIFY `id_akademika` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pro tabulku `firma`
--
ALTER TABLE `firma`
  MODIFY `id_firmy` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pro tabulku `projekt`
--
ALTER TABLE `projekt`
  MODIFY `id_projektu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pro tabulku `rozsah_projektu`
--
ALTER TABLE `rozsah_projektu`
  MODIFY `id_rozsah_projektu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `spravce`
--
ALTER TABLE `spravce`
  MODIFY `id_spravce` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `stitky`
--
ALTER TABLE `stitky`
  MODIFY `id_stitku` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pro tabulku `student`
--
ALTER TABLE `student`
  MODIFY `id_studenta` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pro tabulku `zadavatel_hodnotitel`
--
ALTER TABLE `zadavatel_hodnotitel`
  MODIFY `id_zadavatele_hodnotitele` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `firma`
--
ALTER TABLE `firma`
  ADD CONSTRAINT `fk_firma_akademik` FOREIGN KEY (`id_akademika`) REFERENCES `akademik` (`id_akademika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `firma_rozsah`
--
ALTER TABLE `firma_rozsah`
  ADD CONSTRAINT `fk_table1_firma1` FOREIGN KEY (`id_firmy`) REFERENCES `firma` (`id_firmy`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_table1_rozsah_projektu1` FOREIGN KEY (`id_rozsahu_projektu`) REFERENCES `rozsah_projektu` (`id_rozsah_projektu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `firma_zastresovatel`
--
ALTER TABLE `firma_zastresovatel`
  ADD CONSTRAINT `fk_firma_zastresovatel_firma1` FOREIGN KEY (`id_firmy`) REFERENCES `firma` (`id_firmy`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_firma_zastresovatel_projekt1` FOREIGN KEY (`id_projektu`) REFERENCES `projekt` (`id_projektu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `hodnoceni`
--
ALTER TABLE `hodnoceni`
  ADD CONSTRAINT `fk_hodnoceni_projekt1` FOREIGN KEY (`id_projektu`) REFERENCES `projekt` (`id_projektu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hodnoceni_zadavatel_hodnotitel1` FOREIGN KEY (`id_zadavatele_hodnotitele`) REFERENCES `zadavatel_hodnotitel` (`id_zadavatele_hodnotitele`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `projekt`
--
ALTER TABLE `projekt`
  ADD CONSTRAINT `fk_projekt_akademik1` FOREIGN KEY (`id_akademika`) REFERENCES `akademik` (`id_akademika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projekt_rozsah_projektu1` FOREIGN KEY (`id_rozsahu_projektu`) REFERENCES `rozsah_projektu` (`id_rozsah_projektu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projekt_zadavatel_hodnotitel1` FOREIGN KEY (`id_zadavatele`) REFERENCES `zadavatel_hodnotitel` (`id_zadavatele_hodnotitele`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `projekt_stitky`
--
ALTER TABLE `projekt_stitky`
  ADD CONSTRAINT `fk_projekt_stitky_projekt1` FOREIGN KEY (`id_projektu`) REFERENCES `projekt` (`id_projektu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projekt_stitky_stitky1` FOREIGN KEY (`id_stitku`) REFERENCES `stitky` (`id_stitku`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `student_resitel`
--
ALTER TABLE `student_resitel`
  ADD CONSTRAINT `fk_student_resitel_projekt1` FOREIGN KEY (`projekt_id_projektu`) REFERENCES `projekt` (`id_projektu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_student_resitel_student1` FOREIGN KEY (`student_id_studenta`) REFERENCES `student` (`id_studenta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omezení pro tabulku `zadavatel_hodnotitel`
--
ALTER TABLE `zadavatel_hodnotitel`
  ADD CONSTRAINT `fk_zadavatel_hodnotitel_akademik1` FOREIGN KEY (`id_akademika`) REFERENCES `akademik` (`id_akademika`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_zadavatel_hodnotitel_firma1` FOREIGN KEY (`id_firmy`) REFERENCES `firma` (`id_firmy`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_zadavatel_hodnotitel_student1` FOREIGN KEY (`id_studenta`) REFERENCES `student` (`id_studenta`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
